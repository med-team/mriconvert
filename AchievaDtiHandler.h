  ///
  /**
    */

#ifndef ACHIEVA_DTI_HANDLER_H
#define ACHIEVA_DTI_HANDLER_H

#include "SeriesHandler.h"

namespace jcs {

  ///
  /**
    */
  class AchievaDtiHandler : public SeriesHandler
  {
  public:
    AchievaDtiHandler(const std::string& seriesUid);
    virtual GradientInfo GetGradientInfo();
    virtual bool IsDti() const { return true; }

  protected:
    virtual VolListType ReadVolIds(DicomFile& file);

  private:
    std::vector<std::vector<double> > gradients;

  };
};

#endif
