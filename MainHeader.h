/// MainHeader.h
/**
*/

#ifndef MAINHEADER_H
#define MAINHEADER_H

#include <string>
#include <vector>
#include <istream>
#include <wx/wx.h>

#include "Dictionary.h"
#include "DicomTags.h"
#include "DicomElementInstance.h"

namespace jcs {
  struct MainHeader {
    wxUint8 transferSyntaxCode;
    std::vector<DicomElementInstance> mhElements;
    bool isPixelData(DicomElementInstance de);
    friend std::istream &operator>>(std::istream &in, MainHeader &mh);
  };
  std::istream &operator>>(std::istream &in, MainHeader &mh);
}

#endif
