/// NiftiOutputter.cpp
/**
*/

#include "NiftiOutputter.h"

using namespace jcs;


///
/*
*/
NiftiOutputter::NiftiOutputter() : 
NiftiOutputterBase(CreateOptions())
{
}


///
/*
*/
NiftiOutputter::~NiftiOutputter()
{
}


///
/*
*/
Options
NiftiOutputter::CreateOptions()
{
  Options options = Get3DOptions();
  options.pathname = "Nifti";
  return options;
}


///
/* Checks data type (BitsAllocated and PixelRepresentation) 
    and creates a *Conversion instance. Runs the Convert
    method of that instance.
    \param handler The SeriesHandler instance for the data.
    \return 1
*/
int
NiftiOutputter::ConvertSeries(SeriesHandler* handler)
{
  int bits_allocated, pixel_rep = 0;
  handler->Find("BitsAllocated", bits_allocated);
  handler->Find("PixelRepresentation", pixel_rep);

  switch (bits_allocated + pixel_rep) {

  case 8 : {
      NiftiConversion<wxUint8> conversion(this, handler);
      conversion.Convert();
      break;
    }
  case 9 : {
      NiftiConversion<wxInt8> conversion(this, handler);
      conversion.Convert();
      break;
    }
  case 16 : {
      NiftiConversion<wxUint16> conversion(this, handler);
      conversion.Convert();
      break;
    }
  case 17 : {
      NiftiConversion<wxInt16> conversion(this, handler);
      conversion.Convert();
      break;
    }
  case 32 : {
      NiftiConversion<wxUint32> conversion(this, handler);
      conversion.Convert();
      break;
    }
  case 33 : {
      NiftiConversion<wxInt32> conversion(this, handler);
      conversion.Convert();
      break;
    }
  default : {
      wxLogMessage(_("BitsAllocated and PixelRepresentation values (%d, %d) not supported."), bits_allocated, pixel_rep);
    }
  }
  return 1;
}
