/// McVerter.h
/**
*/

#ifndef MCVERTER_H_
#define MCVERTER_H_

#define NO_CONFIG
// #define _CRT_SECURE_NO_DEPRECATE 1
// #define _CRT_SECURE_NO_WARNINGS 1
#define EXIT_NO_WX -1
#define EXIT_SUCCESS 0
#define EXIT_NO_PROCESSING 1
#define EXIT_NO_INPUT_SPECIFIED 2
#define EXIT_ARGUMENT_ERROR -2
#define EXIT_PERMISSION_ERROR -3

#include <string>
#include <iostream>
#include <vector>
#include <wx/app.h>
#include <wx/cmdline.h>
#include <wx/string.h>
#include <wx/dir.h>
#include <wx/config.h>
#include <wx/stdpaths.h>
#include <wx/tokenzr.h>

#include "OutputFactory.h"
#include "Converter.h"
#include "OutputterBase.h"
#include "StringConvert.h"
#include "Globals.h"
#include "version_string.h"
#include "ConfigValues.h"

#endif
