/// ConfigValues.h
/** Macros for config file entries.
*/

#ifndef CONFIG_VALUES_H
#define CONFIG_VALUES_H

#define CFG_raw "raw"
#define CFG_skip "skip"
#define CFG_split_subj "split_subj"
#define CFG_split_dir "split_dir"
#define CFG_dim "fourd"
#define CFG_ho "ho"
#define CFG_nii "nii"
#define CFG_v16 "v16"
#define CFG_rescale "rescale"
#define CFG_lastdir _T("lastdir")
#define CFG_dir _T("dir")
#define CFG_framewidth _T("framewidth")
#define CFG_frameheight _T("frameheight")
#define CFG_frameposx _T("frameposx")
#define CFG_frameposy _T("frameposy")

#endif
