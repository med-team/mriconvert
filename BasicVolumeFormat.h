/// BasicVolumeFormat.h
/** Handles the task of writing information to header and data files.
*/

#ifndef BASIC_VOLUME_FORMAT_H_
#define BASIC_VOLUME_FORMAT_H_

#include <fstream>
#include <vector>

#include <wx/defs.h>
#include <wx/filename.h>
#include <wx/log.h>

#include "Volume.h"

namespace jcs {

  struct Basic3DHeader {
    virtual ~Basic3DHeader() {}
    virtual void SetNumberOfSlices(int slices) = 0;
    virtual void SetSliceSpacing(double spacing) = 0;
    virtual int GetNumberOfSlices() = 0;
  };

  class BasicVolumeFormat {

    public :
      BasicVolumeFormat(const char* filename, const char* header_extension, const char* raw_extension = "");

      virtual ~BasicVolumeFormat();

      virtual void WriteHeader(Basic3DHeader* header) = 0;
      int AppendRawData(char* data, size_t n_bytes);

    protected:
      wxString mHeaderExtension;
      wxString mRawExtension;
      wxFileName mFileName;
      std::fstream mHeaderFile;
      std::fstream mRawDataFile;
    
      int  mOpenHeaderFile(std::ios::openmode mode);
      void mCloseHeaderFile() { mHeaderFile.close(); }
      int  mReadHeaderFile();
      int  mWriteHeaderFile();

      int  mOpenRawDataFile(std::ios::openmode mode);
      void mCloseRawDataFile() { mRawDataFile.close(); }
  };
}

#endif
