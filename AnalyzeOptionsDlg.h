/// AnalyzeOptionsDlg.h
/**
*/

#ifndef ANALYZE_OPTIONS_DLG_H_
#define ANALYZE_OPTIONS_DLG_H_

#include "BasicOptionsDlg.h"

namespace jcs {
  
  class AnalyzeOutputter;
  
  class AnalyzeOptionsDlg: public BasicOptionsDlg
  {
  public:
    AnalyzeOptionsDlg(AnalyzeOutputter* outputter);
    void OnOkay(wxCommandEvent& event);
    bool NeedsRebuild() const { return mNeedsRebuild; }

  protected:
    DECLARE_EVENT_TABLE()

  private:
    AnalyzeOutputter* mOutputter;
    bool mNeedsRebuild;
  };

}

#endif
