/// MainHeader.cpp
/**
*/

#include "MainHeader.h"

using namespace jcs;

std::istream &
jcs::operator>> (std::istream &in, MainHeader &mh)
{
  DicomElementInstance de;
  de.transferSyntaxCode = mh.transferSyntaxCode;
  std::streampos save_position;
  while (true) {
    save_position = in.tellg();
    in >> de;
    if (mh.isPixelData(de)) {
      in.seekg(save_position);
      break;
    }
    else {
      mh.mhElements.push_back(de);
    }
  }
  return in;
}

bool
MainHeader::isPixelData(DicomElementInstance de)
{
  return (de.tag == DicomTag(0x7fe0, 0x0010)) ? true : false;
}
