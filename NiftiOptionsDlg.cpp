/// NiftiOptionsDlg.cpp
/**
*/

#include <wx/wxprec.h>
#include <wx/spinctrl.h>
#include <wx/statline.h>

#include "NiftiOptionsDlg.h"
#include "NiftiOutputter.h"

using namespace jcs;

BEGIN_EVENT_TABLE(NiftiOptionsDlg, BasicOptionsDlg)

EVT_BUTTON(wxID_OK, NiftiOptionsDlg::OnOkay)

END_EVENT_TABLE()


NiftiOptionsDlg::NiftiOptionsDlg(NiftiOutputterBase* outputter)
: BasicOptionsDlg(_("Nifti options"), outputter),
mOutputter(outputter)
{
  wxBoxSizer* dlgSizer = new wxBoxSizer(wxVERTICAL);

  dlgSizer->Add(myPanel);

  mpNiiCheck = new wxCheckBox(this, -1, _("Save as .nii file"));
  mpNiiCheck->SetValue(mOutputter->saveNii);
  dlgSizer->Add(mpNiiCheck, 0, wxLEFT|wxRIGHT|wxBOTTOM, 10);

  wxBoxSizer* buttonSizer = new wxBoxSizer(wxHORIZONTAL);

  wxButton* okayButton = new wxButton(this, wxID_OK, _("Okay"));
  buttonSizer->Add(okayButton, 0, wxRIGHT, 10);
  buttonSizer->Add(new wxButton(this, wxID_CANCEL, _("Cancel")));
  okayButton->SetDefault();

  dlgSizer->Add(buttonSizer, 0, wxALIGN_RIGHT|wxALL, 10);

  SetSizer(dlgSizer);
  dlgSizer->Fit(this);

  mNeedsRebuild = false;
}

void
NiftiOptionsDlg::OnOkay(wxCommandEvent& event)
{
  bool nfSave = SaveNameFields();
  bool niiSave = (SaveNii() != mOutputter->saveNii);
  bool rbld = Rebuild();
  mNeedsRebuild = nfSave || niiSave || rbld;
  BasicOptionsDlg::OnOkay(event);
  mOutputter->SetSaveNii(SaveNii());
  EndModal(event.GetId());
}
