/// DicomViewer.h
/** Displays DICOM header information in a wxFrame.
*/

#ifndef DICOM_VIEWER_H
#define DICOM_VIEWER_H

#include <string>
#include <wx/fdrepdlg.h>
#include "DicomFile.h"

class wxListView;


class CSAViewer : public wxFrame
{
public:
  CSAViewer(wxWindow* parent, const wxString& title,
  const std::string& header);

private:
  wxTextCtrl* mTextCtrl;
  void AddHeader(const std::string& header);
  void AddNext(std::istream& in);

};

class DicomViewer: public wxFrame
{
public:
  DicomViewer(wxWindow* parent, const char* filename);

  void OnSave(wxCommandEvent& event);
  void OnQuit(wxCommandEvent& event);
  void OnCsaImage(wxCommandEvent& event);
  void OnCsaSeries(wxCommandEvent& event);

  void ShowFindDialog(wxCommandEvent& event);
  void OnFindDialog(wxFindDialogEvent& event);
  void OnCloseFindDialog(wxFindDialogEvent& event);

protected:
  DECLARE_EVENT_TABLE()

private:
  jcs::DicomFile mDicomFile;
  wxListView* mpDicomList;
  void InitList();
  int AddElement(jcs::DicomElement& e);
  wxString mGetItemText(long index, int column) const;
  std::string::size_type mDescLength;

};

#endif
