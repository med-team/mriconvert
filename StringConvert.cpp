/// StringConvert.cpp
/**
*/

#include "StringConvert.h"
#include <cmath>

///
/**
*/
float
jcs::stof(const std::string& rString)
{
  //float retval = 0;
  //if (!rString.empty()) {
  //  retval = stof(rString, 0);
  //}
  //return retval;

  if (rString.empty()) {
    return 0;
  }
  return stof(rString, 0);
}


///
/**
    \param rString std::string reference to convert to float.
    \param n 
    \return A float.
*/
float
jcs::stof(const std::string& rString, int n)
{
  std::stringstream ss;
  ss << rString;
  float f;
  for (int i = 0; i <= n; ++i) {
    ss >> f;
    ss.get();
  }
  return f;
}
  

///
/**
    \param rString std::string reference to convert to int.
    \return An int.
*/
int
jcs::stoi(const std::string& rString)
{
  int value;
  //std::istringstream(rString) >> value;
  std::string tmp_string = rString;
  tmp_string.erase(0, tmp_string.find_first_not_of(' '));
  std::stringstream ss;
  ss << tmp_string;
  ss >> value;
  return value;
}


///
/**
*/
int
jcs::stoi(const std::string& rString, int n)
{
  std::stringstream ss;
  ss << rString;
  int f;
  for (int i = 0; i <= n; ++i) {
    ss >> f;
    ss.get();
  }
  return f;
}


///
/**
*/
std::string
jcs::itos(const int i, int width)
{
  std::stringstream ss;
  ss.fill('0');
  ss.width(width);
  ss << i;
  std::string s;
  ss >> s;
  return s;
}


///
/**
*/
std::string
jcs::ftos(const double f)
{
  std::stringstream ss;
  ss << f;
  std::string str;
  ss >> str;
  return str;
}

///
/**
*/
std::string
jcs::ftos(const double f, int precision)
{
  // c++ 11 and up only
  // f = round(f*pow(10.0,precision))/pow(10.0,precision);

  double df = ceil( (f*pow(10.0, precision)) - 0.49 ) / pow(10.0, precision);
  std::stringstream ss;
  ss << df;
  std::string str;
  ss >> str;
  return str;
}
///
/**
*/
std::string
jcs::Date(const std::string& date)
{
  std::string retval = date;
  if (date.size() > 6) {
    std::string newdate;
    newdate.reserve(10);
    newdate.append(date.begin() + 4, date.begin() + 6);
    newdate.append("/");
    newdate.append(date.begin() + 6, date.end());
    newdate.append("/");
    newdate.append(date.begin(), date.begin() + 4);
    return newdate;
  }
  else {
    return date;
  }
}


///
/**
*/
std::string
jcs::RemoveInvalidChars(const std::string& name)
{
  const std::string forbidden("\\/:*?\"<>|;, .#^");

  // this initialization will drop any NULL terminator
  std::string new_name(name.c_str());
  std::replace_if(
    new_name.begin(),
    new_name.end(),
    std::bind2nd(member_of<char>(), forbidden),
    '_');

  // remove trailing underscores
  std::string::size_type length = new_name.find_last_not_of('_');
  if (length != std::string::npos) {
    length += 1;
    new_name.resize(length);
  }
  return new_name;
}


///
/**
*/
std::vector<std::string>
jcs::ParseDicomString(const std::string& str)
{
  std::vector<std::string> v;
  std::string::size_type pos1 = 0;
  std::string::size_type pos2 = str.find_first_of('/');
  v.push_back(str.substr(0, pos2));
  while (pos2 != std::string::npos) {
    pos1 = pos2 + 1;
    pos2 = str.find_first_of('/', pos1);
    if (pos2 != std::string::npos) {
      int nchars = pos2 - pos1;
      v.push_back(str.substr(pos1, nchars));
    }
    else {
      v.push_back(str.substr(pos1));
    }
  }
  return v;
}
