/// GeEpiHandler.h
/**
    */

#ifndef GE_DTI2_HANDLER_H
#define GE_DTI2_HANDLER_H

#include "GeEpiHandler.h"

namespace jcs {
  ///
  /**
    */
  class GeDti2Handler : public GeEpiHandler
  {
  public:
    GeDti2Handler(const std::string& seriesUid);
    virtual GradientInfo GetGradientInfo();
    virtual bool IsDti() const { return true; }

  protected:
    virtual VolListType ReadVolIds(DicomFile& file);

  private:
    std::vector<std::vector<double> > gradients;

  };
}

#endif
