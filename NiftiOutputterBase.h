/// NiftiOutputterBase.h
/**
*/

#ifndef NIFTI_OUTPUTTER_BASE_H_
#define NIFTI_OUTPUTTER_BASE_H_

#include <string>
#include <map>
#include <math.h>
#include <vector>

#include "Basic3DOutputter.h"
#include "BasicVolumeFormat.h"

namespace jcs {

  /// Provides the basis for both flavors of NIfTi.
  // could be recombined with NiftiOutputter if FSLNifti goes away.
  class NiftiOutputterBase : public Basic3DOutputter {

    public:
    NiftiOutputterBase(const Options& options);
    bool saveNii;
    void SetSaveNii(bool value);

    virtual void SetOption(const std::string& name, bool value);

  protected:
    virtual BasicVolumeFormat* GetOutputVolume(const char* file);

  };
}

#endif
