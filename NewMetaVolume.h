#ifndef NEW_META_VOLUME_H_
#define NEW_META_VOLUME_H_

#include <fstream>
#include <vector>


// MetaIO, from ITK
#include "metaTypes.h"

#include "Globals.h"
#include "Volume.h"
#include "BasicVolumeFormat.h"

namespace jcs {

struct NewMetaHeader: public Basic3DHeader {

	int nDims;
	int dimSize[4];
	int numberOfChannels;
	MET_ValueEnumType elementType;
	double elementSpacing[4];
	double origin[4];
	std::vector<double> orientation;
	bool byteOrderMSB;
	int headerSize;
	std::string elementFile;
	std::vector<std::string> sourceFileVector;
	std::vector<std::string> extraFields;

	virtual void SetNumberOfSlices(int slices) { dimSize[2] = slices; }
	virtual void SetSliceSpacing(double spacing) { elementSpacing[2] = spacing; }
	virtual int GetNumberOfSlices() { return dimSize[2]; }


};

class NewMetaVolume: public BasicVolumeFormat {

public :

	NewMetaVolume(const char* filename,
		      const char* header_extension,
		      const char* raw_extension);

	virtual void WriteHeader(Basic3DHeader* header); 


};

}

#endif
