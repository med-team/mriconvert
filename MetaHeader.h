/// MetaHeader.h
/**
*/

#ifndef METAHEADER_H
#define METAHEADER_H

#include <string>
#include <istream>
#include <iostream>
#include <wx/wx.h>

#include "Dictionary.h"
#include "DicomTags.h"
#include "DicomElementInstance.h"

namespace jcs {
  // Handles group 0x0002 tags.
  struct MetaHeader {
    static const wxUint8 transferSyntaxCode = LEE_CODE;
    std::vector<DicomElementInstance> mhElements;
    bool IsMetaHeaderElement(DicomElementInstance de);
    wxUint8 GetMainTransferSyntaxCode();
    friend std::istream &operator>>(std::istream &in, MetaHeader &mh);
  };
  std::istream &operator>>(std::istream &in, MetaHeader &mh);
}

#endif
