/// Preamble.cpp
/** Encapsulates DICOM preamble.
    \author Chuck Theobald
*/

#include "Preamble.h"

using namespace jcs;


///
/** Reads the input stream assuming a DICOM preamble.
    Also reads the four-character magic sequence.
*/
std::istream &
jcs::operator>> (std::istream &in, Preamble &p)
{
  in.ignore(128);
  in.read(p.buff, sizeof(p.buff));
  p.magic.assign(p.buff);
  return in;
}


///
/** Tests whether the input stream is DICOM.
    \return True if magic identifies a DICOM file stream.
            False if magic is not "DICM".
*/
bool
Preamble::IsDICOM() {
  bool retval = false;
  if (magic == "DICM") {
    retval = true; 
  }
  return retval;
}
