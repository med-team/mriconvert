/// McFrame.cpp
/** Graphical User Interface main window.
*/

// ----------------------------------------------------------------------------
// headers
// ----------------------------------------------------------------------------
// For compilers that support precompilation, includes "wx/wx.h".
#if defined(__WXGTK__) || defined(__WXMOTIF__)
  #include <wx/wx.h>
#endif

#include <string>
#include <fstream>

#include <wx/wxprec.h>
#include <wx/config.h>
#include <wx/url.h>
#include <wx/hyperlink.h>
#include <wx/aboutdlg.h>


#include "StringConvert.h"
#include "MRIConvert.h"
#include "McFrame.h"
#include "McPanel.h"
#include "TextFileViewer.h"
#include "version_string.h"

// ----------------------------------------------------------------------------
// resources
// ----------------------------------------------------------------------------
#include "MRIConvert.xpm"

// ----------------------------------------------------------------------------
// constants
// ----------------------------------------------------------------------------
enum {
  MENU_INPUT_ADD_FILES = 1,
  MENU_INPUT_ADD_DIR,
  MENU_INPUT_REMOVE,
  MENU_INPUT_INFO,
  MENU_INPUT_DICOM,
  MENU_INPUT_IMAGE,
  MENU_CONVERT_NEW,
  MENU_CONVERT_CONVERT,
  MENU_OUTPUT_DIRECTORY,
  MENU_OUTPUT_RENAME,
  MENU_OUTPUT_OPTIONS,
  MENU_OUTPUT_OVERRIDE,
  MENU_HELP
};

BEGIN_EVENT_TABLE(McFrame, wxFrame)

EVT_MENU(MENU_CONVERT_NEW,        McFrame::OnNew)
EVT_MENU(MENU_INPUT_ADD_FILES,    McFrame::OnAddFiles)
EVT_MENU(MENU_INPUT_ADD_DIR,      McFrame::OnAddDir)
EVT_MENU(MENU_OUTPUT_DIRECTORY,   McFrame::OnDirectory)
EVT_MENU(MENU_CONVERT_CONVERT,    McFrame::OnConvert)
EVT_MENU(MENU_INPUT_INFO,         McFrame::OnInfo)
EVT_MENU(MENU_INPUT_DICOM,        McFrame::OnDicom)
EVT_MENU(MENU_INPUT_IMAGE,        McFrame::OnImage)
EVT_MENU(MENU_INPUT_REMOVE,       McFrame::OnRemove)
EVT_MENU(MENU_OUTPUT_RENAME,      McFrame::OnRename)
EVT_MENU(MENU_OUTPUT_OPTIONS,     McFrame::OnOptions)
EVT_MENU(MENU_OUTPUT_OVERRIDE,    McFrame::OnOverride)
EVT_MENU(wxID_ABOUT,              McFrame::OnAbout)
EVT_MENU(MENU_HELP,               McFrame::OnHelp)
EVT_MENU(wxID_EXIT,               McFrame::OnQuit)

END_EVENT_TABLE()

// ----------------------------------------------------------------------------
// main frame
// ----------------------------------------------------------------------------

// frame constructor
McFrame::McFrame(const wxString& title, const wxPoint& pos, const wxSize& size)
: wxFrame((wxFrame *)NULL, -1, title, pos, size)
{
  SetIcon(wxIcon(MRIConvert));

  CreateStatusBar();

  wxMenuBar* pMenuBar = new wxMenuBar();

  wxMenu* pInputMenu = new wxMenu();
  pInputMenu->Append(MENU_INPUT_ADD_FILES, _("&Add Files\tCtrl+A"), _("Add files to convert"));
  pInputMenu->Append(MENU_INPUT_ADD_DIR, _("Add &Folder\tCtrl+F"), _("Add directory of files to convert"));
  pInputMenu->Append(MENU_INPUT_REMOVE, _("&Remove\tCtrl+R"), _("Remove files"));

  pInputMenu->AppendSeparator();
  pInputMenu->Append(MENU_INPUT_INFO, _("&Series Info\tCtrl+S"), _("View series information"));
  pInputMenu->AppendSeparator();
  pInputMenu->Append(MENU_INPUT_DICOM, _("View &DICOM\tCtrl+D"), _("View DICOM file as text"));
  pInputMenu->Append(MENU_INPUT_IMAGE, _("View &Image\tCtrl+I"), _("View image from DICOM file"));
  pMenuBar->Append(pInputMenu, _("&Input"));

  wxMenu* pOutputMenu = new wxMenu();
  pOutputMenu->Append(MENU_OUTPUT_DIRECTORY, _("&Directory\tAlt+D"), _("Set output directory"));
  pOutputMenu->Append(MENU_OUTPUT_RENAME, _("&Rename\tAlt+R"), _("Rename output files/directories"));
  pOutputMenu->Append(MENU_OUTPUT_OPTIONS, _("&Options\tAlt+O"), _("Set default options for this format"));
  pOutputMenu->Append(MENU_OUTPUT_OVERRIDE, _("Override"), _("Override default options for this format"));
  pMenuBar->Append(pOutputMenu, _("&Output"));

  wxMenu* pConvertMenu = new wxMenu();
  pConvertMenu->Append(MENU_CONVERT_CONVERT, _("&Convert All\tCtrl+C"), _("Convert all files"));
  pConvertMenu->Append(MENU_CONVERT_NEW, _("&New session\tCtrl+N"), _("Start new session"));
  pConvertMenu->Append(wxID_EXIT, _("E&xit\tCtrl+X"), _("Quit the application"));
  pMenuBar->Append(pConvertMenu, _("&Convert"));

  wxMenu* pHelpMenu = new wxMenu();
  pHelpMenu->Append(MENU_HELP, _("File formats"), _("About output file formats"));
  pHelpMenu->Append(wxID_ABOUT, _("&About"), _("About MRIConvert"));
  pMenuBar->Append(pHelpMenu, _("&Help"));

  SetMenuBar(pMenuBar);

  mpPanel = new McPanel(this);

  wxBoxSizer* sizer = new wxBoxSizer(wxVERTICAL);
  sizer->Add(mpPanel, 1, wxEXPAND);
  SetSizer(sizer);
  sizer->SetSizeHints(this);
  SetSize(size);
  Move(pos);
}


///
/**
 */
McFrame::~McFrame()
{
  wxConfig::Get()->Write(CFG_framewidth, GetSize().GetWidth());
  wxConfig::Get()->Write(CFG_frameheight, GetSize().GetHeight());
  wxConfig::Get()->Write(CFG_frameposx, GetPosition().x);
  wxConfig::Get()->Write(CFG_frameposy, GetPosition().y);
}




///
/**
 */
void
McFrame::OnAddDir(wxCommandEvent& event)
{
  wxCommandEvent nullEvent(0,0);
  mpPanel->OnAddDir(nullEvent);
}


///
/**
 */
void
McFrame::OnAddFiles(wxCommandEvent& event)
{
  wxCommandEvent nullEvent(0,0);
  mpPanel->OnAddFiles(nullEvent);
}


///
/**
 */
void
McFrame::OnDicom(wxCommandEvent& event)
{
  wxCommandEvent nullEvent(0,0);
  mpPanel->OnDicom(nullEvent);
}


///
/**
 */
void
McFrame::OnImage(wxCommandEvent& event)
{
  wxCommandEvent nullEvent(0,0);
  mpPanel->OnImage(nullEvent);
}


///
/**
 */
void
McFrame::OnInfo(wxCommandEvent& event)
{
  wxCommandEvent nullEvent(0,0);
  mpPanel->OnInfo(nullEvent);
}


///
/**
 */
void
McFrame::OnRemove(wxCommandEvent& event)
{
  wxCommandEvent nullEvent(0,0);
  mpPanel->OnRemove(nullEvent);
}


///
/**
 */
void
McFrame::OnDirectory(wxCommandEvent& event)
{
  wxCommandEvent nullEvent(0,0);
  mpPanel->OnDirectory(nullEvent);
}


///
/**
 */
void
McFrame::OnNew(wxCommandEvent& event)
{
  wxCommandEvent nullEvent(0,0);
  mpPanel->OnNew(nullEvent);
}


///
/**
 */
void
McFrame::OnConvert(wxCommandEvent& event)
{
  wxCommandEvent nullEvent(0,0);
  mpPanel->OnConvert(nullEvent);
}


///
/**
 */
void
McFrame::OnRename(wxCommandEvent& event)
{
  wxCommandEvent nullEvent(0,0);
  mpPanel->OnRename(nullEvent);
}


///
/**
 */
void
McFrame::OnOptions(wxCommandEvent& event)
{
  wxCommandEvent nullEvent(0,0);
  mpPanel->OnOptions(nullEvent);
}


///
/**
 */
void
McFrame::OnOverride(wxCommandEvent& event)
{
  mpPanel->OnMenuOverride();
}


///
/**
 */
void
McFrame::OnAbout(wxCommandEvent& event)
{
  wxAboutDialogInfo info;
  info.SetName("MRIConvert");
  info.SetVersion(_(VERSION_STRING));
  info.SetDescription(_("MRIConvert converts DICOM files to other useful formats."));
  info.SetCopyright("(C) 2013 University of Oregon\nLewis Center for Neuroimaging");
  info.AddDeveloper("Jolinda Smith");
  info.AddDeveloper("Chuck Theobald");
  //info.AddTranslator("xxxx xxxxxxx");
  info.SetWebSite("mailto:mriconvert@ithelp.uoregon.edu", _("Email Tech Support"));
  wxAboutBox(info);
}


///
/**
 */
void
McFrame::OnHelp(wxCommandEvent& event)
{
  TextFileViewer *view = new TextFileViewer(this, _("About file formats"));
  view->Show();
}


///
/**
 */
void 
McFrame::OnQuit(wxCommandEvent& event)
{
  Close(true);
}
