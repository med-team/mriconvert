/// jcsTree.h
/**
*/

#ifndef JCS_TREE_H
#define JCS_TREE_H

#include <wx/treectrl.h>

#include <string>
#include <map>

class jcsTreeItemData : public wxTreeItemData
{
public:

	jcsTreeItemData(const std::string& uid, int itemType)
	: mUid(uid), mItemType(itemType) {}

	int GetItemType() const { return mItemType; }
	const std::string& GetUid() const { return mUid; }
	const std::string& GetSeriesUid() const { return mSeriesUid; }
	void SetSeriesUid(const std::string& uid) { mSeriesUid = uid; }
	const std::string& GetPrefix() const { return mPrefix; }
	void SetPrefix(const std::string& prefix) { mPrefix = prefix; }

private:

	jcsTreeItemData() {}
	std::string mSeriesUid; // only used for output files
	std::string mPrefix;    // also only for output files
	std::string mUid;
	int mItemType;

};


class jcsTreeCtrl : public wxTreeCtrl
{
public:

	jcsTreeCtrl(wxWindow* parent, wxWindowID id, long style,
	const char* root);

	int GetItemType(const wxTreeItemId& item) const;
	int GetSelectionType() const;
	const std::string GetItemUid(const wxTreeItemId& item) const;
	wxString GetStringSelection() const;
	wxString GetStringSelection(const wxTreeItemId& item) const;
	std::string GetSelectionUid() const;
	wxTreeItemId GetTreeId(std::string uid) const;
	const std::string GetSeriesUid(const wxTreeItemId& item) const;
	const std::string GetPrefix(const wxTreeItemId& item) const;

	bool IsChild(const wxTreeItemId& item, 
	const wxTreeItemId& parent) const;

	void Reset();
	void RemoveBranch(wxTreeItemId branch);
	bool IsMultiple() const;
	int GetSelected(wxArrayTreeItemIds& selecteds) const;

protected:

	//Maps unique id's to tree item id's
	typedef std::map<std::string, wxTreeItemId> jcsTreeMap;
	jcsTreeMap mTreeMap;

private:

	bool HasVisibleRoot();
	void mRemoveFromMap(wxTreeItemId branch);
	void CreateImageList(int size = 16);
	long mStyle;
};

enum {
	dtROOT,
	dtSUBJECT,
	dtSTUDY,
	dtSERIES,
	dtFILE
};


enum {
	folder_icon,
	open_folder_icon,
	file_icon
};

#endif

