/// BvOptionsDlg.cpp
/**
*/

#include <wx/wxprec.h>
#include <wx/spinctrl.h>
#include <wx/statline.h>

#include "BvOptionsDlg.h"

using namespace jcs;

BEGIN_EVENT_TABLE(BvOptionsDlg, wxDialog)

EVT_BUTTON(wxID_OK, BvOptionsDlg::OnOkay)

END_EVENT_TABLE()


BvOptionsDlg::BvOptionsDlg(NewBvOutputter* outputter)
: wxDialog(NULL, -1, wxString(_("BrainVoyager options"))), 
  mOutputter(outputter)
{
  wxBoxSizer* dlgSizer = new wxBoxSizer(wxVERTICAL);

  dlgSizer->Add(new wxStaticText(this, -1, _("Items for default file names")),
		0, wxTOP|wxLEFT|wxRIGHT, 10);

  mpNameFields = new wxCheckListBox(this, -1);

  OutputterBase::FieldMap::iterator it = mOutputter->defaultNameFields.begin();
  OutputterBase::FieldMap::iterator it_end = mOutputter->defaultNameFields.end();
  int i = 0;
  while (it != it_end) {
    mpNameFields->Append(wxString(it->second.name.c_str(), 
				  wxConvLocal));
    mpNameFields->Check(i, it->second.value);
    ++i;
    ++it;
  }

  // default min size has extra white space vertically
  wxSize boxSize = mpNameFields->GetEffectiveMinSize();
  boxSize.SetHeight(boxSize.GetHeight()*3/4);
  mpNameFields->SetMinSize(boxSize);

  dlgSizer->Add(mpNameFields, 0, wxEXPAND|wxALL, 10);


  wxBoxSizer* bvSizer = new wxBoxSizer(wxHORIZONTAL);
  bvSizer->Add(new wxStaticText(this, -1, _("Skip volumes:")), 0, wxTOP|wxALIGN_CENTER, 5);
  mpBvSpin = new wxSpinCtrl(this, -1, _T("0"));
  mpBvSpin->SetValue(mOutputter->mSkip);
  bvSizer->Add(mpBvSpin);

  dlgSizer->Add(bvSizer, 0, wxALL, 10);

  mpBvCheck = new wxCheckBox(this, -1, _("Save .v16 file"));
  mpBvCheck->SetValue(mOutputter->mSaveV16);
  dlgSizer->Add(mpBvCheck, 0, wxALL, 10);

  mpSplitDirsCheck = new wxCheckBox(this, -1, _("Save each series in separate directory"));
  mpSplitDirsCheck->SetValue(mOutputter->GetSplit());
  dlgSizer->Add(mpSplitDirsCheck, 0, wxLEFT|wxRIGHT|wxBOTTOM, 10);

  mpSplitSubjCheck = new wxCheckBox(this, -1, _("Save each subject in separate directory"));
  mpSplitSubjCheck->SetValue(mOutputter->GetSplitSubj());
  dlgSizer->Add(mpSplitSubjCheck, 0, wxLEFT|wxRIGHT|wxBOTTOM, 10);


  wxBoxSizer* buttonSizer = new wxBoxSizer(wxHORIZONTAL);

  wxButton* okayButton = new wxButton(this, wxID_OK, _("Okay"));
  buttonSizer->Add(okayButton, 0, wxRIGHT, 10);
  buttonSizer->Add(new wxButton(this, wxID_CANCEL, _("Cancel")));
  okayButton->SetDefault();

  dlgSizer->Add(buttonSizer, 0, wxALIGN_RIGHT|wxALL, 10);

  SetSizer(dlgSizer);
  dlgSizer->Fit(this);

  mNeedsRebuild = false;
}


void
BvOptionsDlg::OnOkay(wxCommandEvent& event)
{
  bool splitDirs = (SplitDirs() != mOutputter->GetSplit());
  bool splitSubj = (SplitSubj() != mOutputter->GetSplitSubj());
  bool saveV16 = (SaveV16() != mOutputter->mSaveV16);
  bool saveSkip = (Skip() != mOutputter->mSkip);
  bool nfSave = SaveNameFields();

  mNeedsRebuild =  splitDirs || splitSubj || saveV16 || saveSkip || nfSave;

  mOutputter->SetSplit(SplitDirs());
  mOutputter->SetSplitSubj(SplitSubj());
  mOutputter->mSaveV16 = SaveV16();
  mOutputter->mSkip = Skip();

  EndModal(event.GetId());
}


bool
BvOptionsDlg::SaveNameFields()
{
  bool needsRebuild = false;
  for (unsigned int i = 0; i < mpNameFields->GetCount(); ++i) {
    std::string name(mpNameFields->GetString(i).mb_str(wxConvLocal));
    bool value = mpNameFields->IsChecked(i);
    OutputterBase::FieldMap::iterator it = mOutputter->defaultNameFields.begin();
    OutputterBase::FieldMap::iterator it_end = mOutputter->defaultNameFields.end();
    for (;it != it_end; ++it) {
      if (it->second.name == name) {
        if (it->second.value != value) {
          needsRebuild = true;
        }
        it->second.value = value;
        break;
      }
    }
  }
  return needsRebuild;
}

bool
BvOptionsDlg::SplitDirs() const 
{ 
  return mpSplitDirsCheck->GetValue();
}

bool
BvOptionsDlg::SplitSubj() const 
{ 
  return mpSplitSubjCheck->GetValue();
}

bool 
BvOptionsDlg::SaveV16() const
{ 
  return mpBvCheck->GetValue(); 
}

int 
BvOptionsDlg::Skip() const 
{ 
  return mpBvSpin->GetValue(); 
}
