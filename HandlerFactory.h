/// HandlerFactory.h
/**
*/

#ifndef HANDLER_FACTORY_H_
#define HANDLER_FACTORY_H_

#include "DicomFile.h"
#include "SeriesHandler.h"

namespace jcs {

  class HandlerFactory
  {
  public:
    static SeriesHandler* CreateHandler(const char* filename);
  private:
    static int GetSeriesType(DicomFile& file);
  };

}

#endif
