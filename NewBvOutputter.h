#ifndef NEW_BV_OUTPUTTER_H_
#define NEW_BV_OUTPUTTER_H_

#include <string>
#include <map>
#include <limits>

#include "Volume.h"
#include "BvFiles.h"
#include "OutputterBase.h"

namespace jcs {

struct VolInfo;

class NewBvOutputter : public OutputterBase {

public :

  NewBvOutputter();
  virtual ~NewBvOutputter();

  virtual int ConvertSeries(SeriesHandler* handler);
  virtual void RemoveSeries(const std::string& seriesUid);
  virtual void UpdateOutputForSeries(SeriesHandler* handler);
  int mSkip;
  bool mSaveV16;

  virtual void SetOption(const std::string& name, int value);
  virtual void SetOption(const std::string& name, bool value);

private :

  template <class T>
  void mConvertAnat(SeriesHandler* handler);
  
  void mConvertFmr(SeriesHandler* handler);
  
  template <class T>
  void mConvertStc(SeriesHandler* handler);

  static Options CreateOptions();


};

}

 #include "NewBvOutputter.txx"

#endif
