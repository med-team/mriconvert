/// NoOptionsDlg.cpp
/**
*/

#include <wx/wxprec.h>
#include <wx/spinctrl.h>
#include <wx/statline.h>

#include "NoOptionsDlg.h"


using namespace jcs;

BEGIN_EVENT_TABLE(NoOptionsDlg, wxDialog)

EVT_BUTTON(wxID_OK, NoOptionsDlg::OnOkay)

END_EVENT_TABLE()

///
/** We should never see this dialog, as all output formats have options.
 */
NoOptionsDlg::NoOptionsDlg()
: wxDialog(NULL, -1, wxString(_("Output format options")))
{
  wxBoxSizer* dlgSizer = new wxBoxSizer(wxVERTICAL);

  dlgSizer->Add(new wxStaticText(this, -1, 
				 _("No options exist for this format.")), 0, wxALL, 10);

  dlgSizer->Add(new wxButton(this, wxID_OK, _("Okay")), 
		0, wxALIGN_RIGHT|wxALL, 10);

  SetSizer(dlgSizer);
  dlgSizer->Fit(this);

}

void
NoOptionsDlg::OnOkay(wxCommandEvent& event)
{
  EndModal(event.GetId());
}
