/// MRIConvert.h
/**
*/

#ifndef MRICONVERT_H_
#define MRICONVERT_H_

// #define _CRT_SECURE_NO_WARNINGS
// #define _CRT_SECURE_NO_DEPRECATE 1

#include <wx/app.h>

class MRIConvert : public wxApp
{
  public:
    virtual bool OnInit();
  protected:
    wxLocale m_locale; 
};

DECLARE_APP(MRIConvert)

#endif

