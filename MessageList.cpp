/// MessageList.cpp
/**
 */

#if defined(__WXGTK__) || defined(__WXMOTIF__)
  #include <wx/wx.h>
#endif

#include <fstream>

#include <wx/wx.h>
#include <wx/wxprec.h>
#include <wx/listctrl.h>
#include <wx/types.h>

#include "MessageList.h"

using namespace jcs;

BEGIN_EVENT_TABLE(MessageListDlg, wxDialog)

EVT_BUTTON(wxID_SAVE, MessageListDlg::OnSave)

END_EVENT_TABLE()

///
/**
 */
MessageListDlg::MessageListDlg(const wxString& title)
: wxDialog(NULL, -1, title, wxDefaultPosition, wxDefaultSize,
           wxDEFAULT_DIALOG_STYLE|wxRESIZE_BORDER)
{
  wxBoxSizer* dlgSizer = new wxBoxSizer(wxVERTICAL);

  mpListView = new wxListView(this, -1,
                              wxDefaultPosition, wxSize(600, 400), wxLC_REPORT);

  dlgSizer->Add(mpListView, 1, wxEXPAND|wxALL, 20);

  wxBoxSizer* buttonSizer = new wxBoxSizer(wxHORIZONTAL);
  //  buttonSizer->Add(new wxButton(this, wxID_SAVE, _T("Save")), 0, wxRIGHT, 20);
  buttonSizer->Add(new wxButton(this, wxID_OK, _T("Okay")), 0);
  dlgSizer->Add(buttonSizer, 0, wxRIGHT | wxBOTTOM | wxALIGN_RIGHT, 20);

  SetSizer(dlgSizer);
  dlgSizer->Fit(this);
}


///
/**
 */
void
MessageListDlg::SetColumns(const Message& columns)
{
  for (unsigned int i = 0; i < columns.size(); ++i)
    mpListView->InsertColumn(i, columns[i].c_str());
}


///
/**
 */
void
MessageListDlg::AddMessages(const MessageList& messages)
{
  MessageList::const_iterator list_it = messages.begin();
  MessageList::const_iterator list_end = messages.end();
  while (list_it != list_end) {
    int row = mpListView->GetItemCount();
    int column = 0;
    Message::const_iterator it = list_it->begin();
    Message::const_iterator it_end = list_it->end();
    if (mpListView->GetColumnCount() <= column) {
      mpListView->InsertColumn(column, _T(""));
    }
    mpListView->InsertItem(row, it->c_str());
    ++column;
    ++it;
    while (it != it_end) {
      if (mpListView->GetColumnCount() <= column) {
        mpListView->InsertColumn(column, _T(""));
      }
      mpListView->SetItem(row, column, it->c_str());
      ++column;
      ++it;
    }
    ++list_it;
  }

  for (int i = 0; i < mpListView->GetColumnCount(); ++i) {
    mpListView->SetColumnWidth(i, wxLIST_AUTOSIZE);
    if (mpListView->GetColumnWidth(i) > 450) {
      mpListView->SetColumnWidth(i, 450);
    }
  }
}


///
/**
 */
void
MessageListDlg::OnSave (wxCommandEvent& event)
{
  wxFileDialog* dlg = new wxFileDialog(this,
                                       _T("Choose a file"), _T(""), _T(""), _T("*.txt"),
                                          wxFD_SAVE);

  if (dlg->ShowModal() == wxID_OK) {
    wxString path = dlg->GetPath();
    std::ofstream output(path.mb_str(wxConvLocal));

    for (int i = 0; i < mpListView->GetItemCount(); ++i) {
      for (int j = 0; j < mpListView->GetColumnCount(); ++j) {
        output << mGetItemText(i, j).mb_str(wxConvLocal);
        output << '\t';
      }
      output << std::endl;
    }
    output.close();
  }
  dlg->Destroy();
}


///
/**
 */
wxString
MessageListDlg::mGetItemText(long index, int column) const
{
  wxListItem item;
  item.SetId(index);
  item.SetColumn(column);
  item.SetMask(wxLIST_MASK_TEXT);
  mpListView->GetItem(item);
  return item.GetText();
}
