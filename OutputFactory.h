/// OutputFactory.h
/**
 */

#ifndef OUTPUT_FACTORY_H_
#define OUTPUT_FACTORY_H_

#include <string>

#include "NewSpmOutputter.h"
#include "NewMetaOutputter.h"
#include "NewBvOutputter.h"
#include "AnalyzeOutputter.h"
#include "NiftiOutputter.h"
#include "FslNiftiOutputter.h"

namespace jcs {

class OutputterBase;
class SeriesHandler;

class OutputFactory
{
public :

	static OutputterBase* CreateNewOutputter(int type);
	static const char* GetDescription(int type);
	static const char* GetShortDescription(int type);
	static int GetNumberOfTypes() { return NUM_TYPES; }

	enum  { 
		FSL,
		SPM,
		META,
		NIFTI,
		ANALYZE,
		BV,
		NUM_TYPES
	};


};

}


#endif
