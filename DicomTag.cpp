/// DicomTag.cpp
/**
*/

#include "DicomTag.h"

using namespace jcs;

///
/** DicomTags are equal iff both group numbers and element numbers are equal.
*/
bool
DicomTag::operator== (const DicomTag& rhs) const 
{
  return((group == rhs.group) && (element == rhs.element));
}


///
/** DicomTags are not equal if the object references are not equal.
    This is not the same as for the equality operator, a future 
    implementation in a better world may change this behavior.
*/
bool
DicomTag::operator!= (const DicomTag& rhs) const 
{
  return !(*this == rhs);
}


///
/** A DicomTag is less than another DicomTag if its group number is less than
    the other's group number.  If group numbers are equal, then less than is
    determined by the element numbers.
*/
bool
DicomTag::operator< (const DicomTag& rhs) const
{
  return((group < rhs.group) || ((group == rhs.group) && (element < rhs.element)));
}


///
/** Same as with < operator, but with opposite sense.
*/
bool
DicomTag::operator> (const DicomTag& rhs) const
{
  return((group > rhs.group) || ((group == rhs.group) && (element > rhs.element)));
}


///
/** Read DicomTag from an input stream. Uses istream::in because
    stream input operator does not stop after reading sizeof(dt.group)
    (sizeof(wxUint16)) on its own.
*/
std::istream &
jcs::operator>> (std::istream &in, DicomTag &dt)
{
  in.read((char *)&(dt.group), sizeof(dt.group));
  in.read((char *)&(dt.element), sizeof(dt.element));
  if (dt.transferSyntaxCode == BEE_CODE) {
    wxUINT16_SWAP_ON_LE(dt.group);
    wxUINT16_SWAP_ON_LE(dt.element);
  }
  else {
    wxUINT16_SWAP_ON_BE(dt.group);
    wxUINT16_SWAP_ON_BE(dt.element);
  }
  return in;
}


///
/** This is how we stream out a DicomTag.  Form should be like
    '(xxxx,xxxx)', where xxxx is a hexadecimal number.
*/
std::ostream &
jcs::operator<< (std::ostream& out, const DicomTag& rhs)
{
  out << std::hex << '(';
  out.fill('0');
  out.width(4);
  out << rhs.group << ',';
  out.width(4); 
  out << rhs.element << ')';
  out << std::dec;
  return out;
}
