/// Globals.h
/** Variables available everywhere.
*/

#ifndef _GLOBALS_H_
#define _GLOBALS_H_

namespace jcs {
  extern bool verbose;
  extern bool quiet;
}

#endif
