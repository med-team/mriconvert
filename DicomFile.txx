/// DicomFile.txx
/** Template methods of DicomFile class.
*/

#ifndef DICOMFILE_TXX_
#define DICOMFILE_TXX_

namespace jcs {

  /// Find dicom element, convert to value of type T.
  /** find
      \param d A DicomElement.
      \param rValue A reference to the target data storage area.
      \return 0 if 'd' is not found. The result of method mReadValue otherwise.
  */
  template <class T> int
  DicomFile::Find(DicomElement d, T& rValue) 
  {
    if (!mInputFile.good()) {
      mInputFile.clear();
    }

    if (!FindElement(d, mInputFile)) {
      return 0;
    }
    mInputFile.seekg(- static_cast<std::streamoff>(d.value_length), std::ios::cur);

    return mReadValue(d, rValue, mInputFile); 
  }


  ///
  /**
      \param s A string to seek, generally the accepted name of a 
      DICOM element as defined in one of the Dictionary classes.
      \param rValue A reference to the target data storage area.
      \return The result of method Find.
  */
  // Find dicom element using string from dicom dict.
  template <class T> int
  DicomFile::Find(const std::string& s, T& rValue) 
  {
    return Find(mDicom->Lookup(s), rValue);
  }

  
  ///
  /**
  */
  template <class T> int
  DicomFile::FindInStream(std::istream& input, DicomElement d, T& rValue) 
  {
    if (!input.good()) {
      input.clear();
    }

    if (!FindElement(d, input)) {
      return 0;
    }
    input.seekg(- static_cast<std::streamoff>(d.value_length), 
    std::ios::cur);

    int retval = mReadValue(d, rValue, input);
    return retval;
  }

  
  ///
  /** Retrieve data_length bytes of pixel data.
      It's up to the caller to send correct type and data_length!
      For multi-frame data, retrieves just one frame.  Frames are numbered from 0.
      Will rescaling according to slope and intercept.
      NOTE: This should support BitsAllocated, BitsStored and HighBit calculations.
      \param data A reference to the receiving vector.
      \param data_length The length of data to retrieve, in bytes.
      \param slope The rescale slope.
      \param intercept The rescale intercept.
      \param frame The index of the frame to retrieve.
      \return 1
  */

  // This is the wrong place to apply slope and intercept. It's been moved to conversion step.
  template <class T> int
  DicomFile::PixelData(std::vector<T>& data, std::streamsize data_length, /*double slope, double intercept, */int frame)
  {
    if (!mInputFile.is_open()) { return 0; }
    mInputFile.clear();

    // Go to start of pixel data and skip to requested frame.
    // Frames are indexed 0-based.
    mInputFile.seekg(GetPixelDataStart());
    mInputFile.seekg(data_length * frame, std::ios::cur);

    data.resize(data_length/sizeof(T));
    mInputFile.read(reinterpret_cast<char*>(&data.front()), data_length);

    // This test is not mathematically necessary, but saves a do-nothing
    // iteration through the data.
//    if ((slope != 1) || (intercept != 0)) {
//      for (typename std::vector<T>::iterator it = data.begin(); it != data.end(); ++it) {
        // Casting avoids compiler warnings.
//        *it = static_cast<T>(static_cast<double>(*it) * slope + intercept);
 // }
//    }

    return 1;
  }


  ///
  /** alt version with buffer, used for rgb image 
  */
  template <class T> int
  DicomFile::PixelData(T* data, std::streamsize data_length)
  {
    if (!mInputFile.is_open()) { return 0; }
    mInputFile.clear();

    mInputFile.seekg(mBeginning);

    DicomElement element;
    mReadNextElement(element, mInputFile);
    while ((mInputFile.good()) && (element.tag != Tag("PixelData"))) {
      mReadNextElement(element, mInputFile);
    } 
    mInputFile.seekg(-data_length, std::ios::cur);

    mInputFile.read(reinterpret_cast<char*>(data), data_length);

    return 1;
  }


  ///
  /** Reads next value matching the vr of 'd' from 'input', sets
      'rValue' to the first string found there.
      The main logic of all Find methods.
      \param d A DicomElement instance.
      \param rValue Reference to destination data element.
      \param input Input stream from which to read.
      \return 1 on success, 0 on failed input or zero-length value_length.
  */
  template <class T> int
  DicomFile::mReadValue(DicomElement d, T& rValue, std::istream& input) 
  {
    if (!input.good() || (d.value_length == 0)) {
      return 0;
    }

    std::vector<std::string> v;
    if (ReadMap.count(d.vr)) {
      v = ReadMap[d.vr](input, d.value_length);
    }
    else {
      v = ReadMap["UN"](input, d.value_length);
    }
    
    // remove leading spaces
    v.front().erase(0, v.front().find_first_not_of(' '));

    std::stringstream ss;
    ss << v.front();
    ss >> rValue;

    return 1;
  }
}

#endif
