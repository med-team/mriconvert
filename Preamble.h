/// Preamble.h
/**
*/

#ifndef PREAMBLE_H
#define PREAMBLE_H

#include <string>
#include <istream>

namespace jcs {
  struct Preamble {
    char buff[4];
    std::string magic;
    bool IsDICOM();
    friend std::istream &operator>>(std::istream &in, Preamble &p);
  };
  std::istream &operator>>(std::istream &in, Preamble &p);
}

#endif
