/// TextFileViewer.cpp
/**
*/

#include <fstream>
#include <sstream>
#include <string>

#include <wx/wx.h>
#include <wx/wxprec.h>

#include "TextFileViewer.h"
#include "fileformats.h"


TextFileViewer::TextFileViewer(wxWindow* parent, const wxChar* caption)
: wxFrame(parent, -1, wxString(caption, wxConvLocal))
{
  wxBoxSizer* frameSizer = new wxBoxSizer(wxVERTICAL);

  mTextCtrl = new wxTextCtrl(this, -1, _T(""), wxDefaultPosition,
    wxSize(400,400), wxTE_MULTILINE | wxTE_READONLY | wxTE_RICH);

  frameSizer->Add(mTextCtrl, 1, wxEXPAND);

  std::istringstream input(fileformats_h, std::istringstream::in);

  if (!input.good()) {
    (*mTextCtrl) << _("problem opening istringstream::in\n");
  }

  std::string str;
  while (getline(input, str)) {
    (*mTextCtrl) << wxString(str.c_str(), wxConvLocal) << _T("\n");
#ifdef __WXMSW__
    (*mTextCtrl) << _T('\n');
#endif
  }

  // Scroll to the top of the text.
  mTextCtrl->SetInsertionPoint(0);
  SetSizer(frameSizer);
  frameSizer->Fit(this);
}
