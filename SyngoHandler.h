/// SyngoHandler.h
/**
*/

#ifndef SYNGO_HANDLER_H
#define SYNGO_HANDLER_H

#include <wx/filename.h>
#include <wx/stdpaths.h>

#include <algorithm>
#include <set>
#include <math.h>
#include <cmath>
#include <string>

#include "Dictionary.h"
#include "DicomFile.h"
#include "StringConvert.h"
#include "Volume.h"
#include "SeriesHandler.h"

namespace jcs {
  class SyngoHandler : public SeriesHandler
  {
  public:
    SyngoHandler(const std::string& seriesUid);

    virtual double GetSliceDuration() const;
    virtual AoCode GetAcquisitionOrder() const;
    virtual double GetVolumeInterval() const;
    virtual GradientInfo GetGradientInfo();
    virtual bool IsDti() const;
    virtual bool IsMoCo() const;
	virtual std::vector<std::string> GetStringInfo();

  protected:
    int ReadCSAImageHeader(const std::string& tag, std::string& value) const;
    int ReadCSASeriesHeader(const std::string& tag, std::string& value) const;
    virtual VolListType ReadVolIds(DicomFile& file);

  };
};

#endif
