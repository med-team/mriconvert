/// NewMetaVolume.cpp
/**
*/

#include <iostream>
#include <string>
#include <vector>

#include <wx/log.h>

#include "StringConvert.h"
#include "NewMetaVolume.h"

using namespace jcs;


///
/**
 */
NewMetaVolume::NewMetaVolume(const char* filename, const char* he, const char* re) 
  : BasicVolumeFormat(filename, he, re)
{
}


///
/** Opens header file, writes contents, and closes.
 */
void
NewMetaVolume::WriteHeader(Basic3DHeader* header_3d)
{ 
  NewMetaHeader* header = dynamic_cast<NewMetaHeader*>(header_3d);
  if (header == 0) {
    wxLogError(_T("Object not meta header type"));
    return;
  }

  if (!mOpenHeaderFile(std::ios::out)) {
    wxLogError(_T("Cannot create meta image header file %s"), mFileName.GetFullName().c_str());
    return;
  }

  mHeaderFile << "ObjectType = Image" << std::endl;
  mHeaderFile << "NDims = " << header->nDims << std::endl;
  
  mHeaderFile << "DimSize =";
  for (int i = 0; i < header->nDims; ++i) {
    mHeaderFile << " " << header->dimSize[i];
  }
  mHeaderFile << std::endl;

  mHeaderFile << "ElementSpacing =";
  for (int i = 0; i < header->nDims; ++i) {
    mHeaderFile << " " << header->elementSpacing[i];
  }
  mHeaderFile << std::endl;

  mHeaderFile << "Position =";
  for (int i = 0; i < header->nDims; ++i) {
    mHeaderFile << " " << header->origin[i];
  }
  mHeaderFile << std::endl;

  mHeaderFile << "Orientation =";
  for (unsigned int i = 0; i < header->orientation.size(); ++i) {
    mHeaderFile << " " << header->orientation.at(i);
  }
  mHeaderFile << std::endl;

  mHeaderFile << "CenterOfRotation = 0 0 0" << std::endl;
  mHeaderFile << "AnatomicalOrientation = LPS" << std::endl;
  mHeaderFile << "ElementByteOrderMSB = " << header->byteOrderMSB << std::endl;
  mHeaderFile << "ElementNumberOfChannels = " << header->numberOfChannels << std::endl;
  mHeaderFile << "ElementType = " << MET_ValueTypeName[header->elementType] << std::endl;
  mHeaderFile << "HeaderSize = " << header->headerSize << std::endl;

  std::vector<std::string>::const_iterator it = header->extraFields.begin();
  std::vector<std::string>::const_iterator it_end = header->extraFields.end();
  for (; it != it_end; it++) {
    mHeaderFile << *it << std::endl;
  }

  mHeaderFile << "ElementDataFile = " << header->elementFile.c_str() << std::endl;

  it = header->sourceFileVector.begin();
  it_end = header->sourceFileVector.end();
  for (; it != it_end; it++) {
    mHeaderFile << *it << std::endl;
  }
      
  mCloseHeaderFile();

  if (verbose && !quiet) {
    std::cout << "Wrote " << mFileName.GetFullPath() << std::endl;  //.mb_str(wxConvLocal) << std::endl;
  }
}
