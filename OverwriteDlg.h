#ifndef OVERWRITE_DLG_H_
#define OVERWRITE_DLG_H_

#include <wx/wx.h>
#include <wx/dialog.h>

namespace jcs {

class OverwriteDlg : public wxDialog
{
public:
	OverwriteDlg(wxWindow* parent, const char* filename);

	void OnButton(wxCommandEvent& event);

protected:
    DECLARE_EVENT_TABLE()


};

}

#endif

