#####################################################
## CMakeLists.txt for MRIConvert and mcverter
#####################################################

project( MRIConvert )

cmake_minimum_required( VERSION 2.8 )

if( WIN32 )
  message( "WIN32" )
  set( wxWidgets_ROOT_DIR "c:/wxWidgets-3.0.2" )
  set( wxWidgets_LIB_DIR "c:/wxWidgets-3.0.2/lib/vc_lib" )
  set( wxWidgets_CONFIGURATION "mswu" )
  add_definitions( /D_CRT_SECURE_NO_WARNINGS )
  add_definitions( /D_SCL_SECURE_NO_WARNINGS )
  add_compile_options( /EHsc )
  add_compile_options( /MT )
else( WIN32 )
  set( wxWidgets_ROOT_DIR /usr )
  ## Uncomment for gnu debug.
  ##set( CMAKE_CXX_FLAGS " -ggdb ${CMAKE_CXX_FLAGS} " )
endif( WIN32 )

set( wxWidgets_USE_DEBUG OFF )
set( wxWidgets_USE_UNICODE ON )
set( wxWidgets_USE_SHARED OFF )
set( wxWidgets_USE_UNIVERSAL OFF )

set( CMAKE_VERBOSE_MAKEFILE TRUE )
set( EXECUTABLE_OUTPUT_PATH "release" )

## Setup for wxWidgets, order is important, see
## http://docs.wxwidgets.org/stable/wx_librarieslist.html
find_package( wxWidgets REQUIRED adv core base )
if( wxWidgets_FOUND )
  include( ${wxWidgets_USE_FILE} )
endif( wxWidgets_FOUND )

message( "wxWidgets libraries: ${wxWidgets_LIBRARIES}" )
    
message( "System name ${CMAKE_HOST_SYSTEM_NAME}" )

if( ${CMAKE_HOST_APPLE} )
  message( "OS X build" )

  set( CMAKE_OSX_ARCHITECTURES "x86_64")

  set( MACOSX_BUNDLE_INFO_STRING "MRIConvert-mcverter" )
  set( MACOSX_BUNDLE_ICON_FILE "MRIConvert.icns" )
  set( MACOSX_BUNDLE_GUI_IDENTIFIER "edu.uoregon.lcni.MRIConvert" )
  set( MACOSX_BUNDLE_LONG_VERSION_STRING "version 2.1.0" )
  set( MACOSX_BUNDLE_BUNDLE_NAME "MRIConvert" )
  set( MACOSX_BUNDLE_SHORT_VERSION_STRING "2.1.0" )
  set( MACOSX_BUNDLE_BUNDLE_VERSION "2.1" )
  set( MACOSX_BUNDLE_COPYRIGHT "copyright" )

  set_source_files_properties(
        MRIConvert.icns
        PROPERTIES
        MACOSX_PACKAGE_LOCATION Resources
        )
endif( ${CMAKE_HOST_APPLE} )

add_executable( MRIConvert WIN32 MACOSX_BUNDLE
  AchievaDtiHandler.cpp
  AnalyzeOptionsDlg.cpp
  AnalyzeOutputter.cpp
  AnalyzeVolume.cpp
  Basic3DOutputter.cpp
  BasicOptionsDlg.cpp
  BasicVolumeFormat.cpp
  BvFiles.cpp
  BvOptionsDlg.cpp
  Converter.cpp
  DicomElement.cpp
  DicomElementInstance.cpp
  DicomFile.cpp
  DicomTag.cpp
  DicomTree.cpp
  DicomViewer.cpp
  Dictionary.cpp
  EnhancedMrHandler.cpp
  FslNiftiOutputter.cpp
  GeDti2Handler.cpp
  GeDtiRbHandler.cpp
  GeEpiHandler.cpp
  Globals.cpp
  HandlerFactory.cpp
  ImageView.cpp
  InfoFrame.cpp
  MainHeader.cpp
  MRIConvert.cpp
  McFrame.cpp
  McPanel.cpp
  MessageList.cpp
  MetaHeader.cpp
  MetaOptionsDlg.cpp
  NewBvOutputter.cpp
  NewMetaOutputter.cpp
  NewMetaVolume.cpp
  NewSpmOutputter.cpp
  NewSpmVolume.cpp
  NiftiOptionsDlg.cpp
  NiftiOutputter.cpp
  NiftiOutputterBase.cpp
  NiftiVolume.cpp
  NoOptionsDlg.cpp
  NumarisMosaicHandler.cpp
  OutputFactory.cpp
  OutputList.cpp
  OutputTreeNew.cpp
  OutputterBase.cpp
  OverrideDlg.cpp
  OverwriteDlg.cpp
  Preamble.cpp
  SeriesHandler.cpp
  SpmOptionsDlg.cpp
  StringConvert.cpp
  SyngoHandler.cpp
  SyngoMosaicHandler.cpp
  TextFileViewer.cpp
  ValueRepresentations.cpp
  Volume.cpp
  jcsTree.cpp

  AchievaDtiHandler.h
  AnalyzeOptionsDlg.h
  AnalyzeOutputter.h
  AnalyzeVolume.h
  Basic3DOutputter.h
  BasicOptionsDlg.h
  BasicVolumeFormat.h
  BvFiles.h
  BvOptionsDlg.h
  ByteSwap.h
  ConfigValues.h
  Converter.h
  DicomElement.h
  DicomElementInstance.h
  DicomFile.h
  DicomTag.h
  DicomTags.h
  DicomTree.h
  DicomViewer.h
  Dictionary.h
  EnhancedMrHandler.h
  FslNiftiOutputter.h
  GeDti2Handler.h
  GeDtiRbHandler.h
  GeEpiHandler.h
  Globals.h
  HandlerFactory.h
  ImageView.h
  InfoFrame.h
  MainHeader.h
  MRIConvert.h
  McFrame.h
  McPanel.h
  MessageList.h
  MetaHeader.h
  MetaOptionsDlg.h
  NewBvOutputter.h
  NewMetaOutputter.h
  NewMetaVolume.h
  NewSpmOutputter.h
  NewSpmVolume.h
  NiftiOptionsDlg.h
  NiftiOutputter.h
  NiftiOutputterBase.h
  NiftiVolume.h
  NoOptionsDlg.h
  NumarisMosaicHandler.h
  OutputFactory.h
  OutputList.h
  OutputTreeNew.h
  OutputterBase.h
  OverrideDlg.h
  OverwriteDlg.h
  Preamble.h
  ReadStream.h
  SeriesHandler.h
  SpmOptionsDlg.h
  StringConvert.h
  SyngoHandler.h
  SyngoMosaicHandler.h
  TextFileViewer.h
  ValueRepresentations.h
  Volume.h
  jcsTree.h
  nifti1.h
  metaTypes.h
  resource.h
  fileformats.h
  version_string.h
  
  DicomFile.txx
  SeriesHandler.txx
  NewBvOutputter.txx
  Basic3DConversion.txx

  MRIConvert.rc
  MRIConvert.ico
  MRIConvert.icns
)

add_executable( mcverter
  McVerter.cpp
  AchievaDtiHandler.cpp
  AnalyzeOutputter.cpp
  AnalyzeVolume.cpp
  Basic3DOutputter.cpp
  BasicVolumeFormat.cpp
  BvFiles.cpp
  Converter.cpp
  DicomElement.cpp
  DicomElementInstance.cpp
  DicomFile.cpp
  DicomTag.cpp
  Dictionary.cpp
  EnhancedMrHandler.cpp
  FslNiftiOutputter.cpp
  GeDti2Handler.cpp
  GeDtiRbHandler.cpp
  GeEpiHandler.cpp
  Globals.cpp
  HandlerFactory.cpp
  MainHeader.cpp
  MetaHeader.cpp
  NewBvOutputter.cpp
  NewMetaOutputter.cpp
  NewMetaVolume.cpp
  NewSpmOutputter.cpp
  NewSpmVolume.cpp
  NiftiOutputter.cpp
  NiftiOutputterBase.cpp
  NiftiVolume.cpp
  NumarisMosaicHandler.cpp
  OutputFactory.cpp
  OutputList.cpp
  OutputterBase.cpp
  Preamble.cpp
  SeriesHandler.cpp
  StringConvert.cpp
  SyngoHandler.cpp
  SyngoMosaicHandler.cpp
  ValueRepresentations.cpp
  Volume.cpp

  McVerter.h
  AchievaDtiHandler.h
  AnalyzeOutputter.h
  AnalyzeVolume.h
  Basic3DOutputter.h
  BasicVolumeFormat.h
  BvFiles.h
  ByteSwap.h
  ConfigValues.h
  Converter.h
  DicomElement.h
  DicomElementInstance.h
  DicomFile.h
  DicomTag.h
  DicomTags.h
  Dictionary.h
  EnhancedMrHandler.h
  FslNiftiOutputter.h
  GeDti2Handler.h
  GeDtiRbHandler.h
  GeEpiHandler.h
  Globals.h
  HandlerFactory.h
  MainHeader.h
  MetaHeader.h
  NewBvOutputter.h
  NewMetaOutputter.h
  NewMetaVolume.h
  NewSpmOutputter.h
  NewSpmVolume.h
  NiftiOutputter.h
  NiftiOutputterBase.h
  NiftiVolume.h
  NumarisMosaicHandler.h
  OutputFactory.h
  OutputterBase.h
  Preamble.h
  ReadStream.h
  SeriesHandler.h
  StringConvert.h
  SyngoHandler.h
  SyngoMosaicHandler.h
  ValueRepresentations.h
  Volume.h
  nifti1.h
  metaTypes.h
  resource.h
  fileformats.h
  version_string.h
  
  DicomFile.txx
  SeriesHandler.txx
  NewBvOutputter.txx
  Basic3DConversion.txx
)

if( WIN32 )

  target_link_libraries( MRIConvert comctl32 rpcrt4 ${wxWidgets_LIBRARIES} )
  target_link_libraries( mcverter comctl32 rpcrt4 ${wxWidgets_LIBRARIES} )

else( WIN32 )

  target_link_libraries( MRIConvert ${wxWidgets_LIBRARIES} )
  target_link_libraries( mcverter ${wxWidgets_LIBRARIES} )

endif( WIN32 )
