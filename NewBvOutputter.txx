/// NewBvOutputter.txx
/**
 */

#ifndef NEW_BV_OUTPUTTER_TXX
#define NEW_BV_OUTPUTTER_TXX

#include <limits>
#include <typeinfo>

#include "Volume.h"
#include "BvFiles.h"
#include "StringConvert.h"


namespace jcs {

  template <class T> T
  Max(Volume<T> vol)
  {
    typename std::map<double, std::vector<T> >::iterator it = vol.mSliceMap.begin();
    typename std::map<double, std::vector<T> >::iterator end = vol.mSliceMap.end();
    T vol_max = *std::max_element((*it).second.begin(), (*it).second.end());
    while (++it != end) {
      T slice_max = *std::max_element((*it).second.begin(), (*it).second.end());
      vol_max = std::max<T>(vol_max, slice_max);
    }
    return vol_max;
  }

  template <class T> T
  Min(Volume<T> vol)
  {
    typename std::map<double, std::vector<T> >::iterator it = vol.mSliceMap.begin();
    typename std::map<double, std::vector<T> >::iterator end = vol.mSliceMap.end();
    T vol_min = *std::min_element((*it).second.begin(), (*it).second.end());
    while (++it != end) {
      T slice_min = *std::max_element((*it).second.begin(), (*it).second.end());
      vol_min = std::min<T>(vol_min, slice_min);
    }
    return vol_min;
  }

  template<class T> void
  NewBvOutputter::mConvertAnat(SeriesHandler* handler)
  {
    bool write_vmr = true;
    bool write_v16 = mSaveV16;

    if (!write_vmr && !write_v16) { return; }

    typedef std::map <VolId, Volume<T> > vMapType;
    vMapType volumes;
    handler->GetVolumes(volumes);

    // Just to keep things simple, we only convert the first volume.
    typename vMapType::iterator vit = volumes.begin();

    // Calculate scaling factor;
    T data_min, data_max;
    float scaling = 1;
    wxUint8 char_max = std::numeric_limits<wxUint8>::max();
    if (write_vmr) {
      if (verbose) {
        wxLogStatus(_T("Calculating scaling factor..."));
      }   
      data_min = Min((*vit).second);
      data_max = static_cast<T>(Max((*vit).second) * .5);
      if ((data_max - data_min) != 0) {
        scaling = static_cast<float>(char_max)/static_cast<float>(data_max - data_min);
      }
    }

    wxUint16 dim[3];
    dim[0] = handler->GetColumns();  // original: x; BV: -z 
    dim[1] = handler->GetRows();     // original: y; BV: +x
    dim[2] = (*vit).second.size();   // original: z; BV: -y

    int factor[] = {1, 1, 1};
    int offset[] = {0, 0, 0};
    for (int i = 0; i < 3; ++i) {
      while (dim[i] > 256*factor[i]) {
        factor[i] *= 2;
      }
      while ((dim[i] + offset[i]*factor[i]) < 256*factor[i]) {
        ++offset[i];
      }
      assert((dim[i]/factor[i] + offset[i]) == 256);
    }

    std::vector<wxUint8> data(256*256*256);   // for .vmr file
    std::vector<wxInt16> data_16;       // for .v16 file
    if (mSaveV16) {
      data_16 = std::vector<wxInt16>(256*256*256);
    }

    if (verbose) {
      wxLogStatus(_T("Converting data...."));
    }
    typename std::map<double, std::vector<T> >::iterator it = (*vit).second.begin();
    typename std::map<double, std::vector<T> >::iterator last = (*vit).second.end();

    wxUint8* start_point;
    start_point = &data.front() + 255*256   // last line, first slice
      + offset[0]*256*128           // BV z offset
      - offset[2]*128               // BV y offset
      + 256 - offset[1]/2;          // BV x offset 

    wxInt16* start_point_16;
    start_point_16 = &data_16.front() + 255*256 
      + offset[0]*256*128     
      - offset[2]*128       
      + 256 - offset[1]/2;    

    int slice_no = 0;
    while (it != last) {
      T* source_ptr = &((*it).second.front());
      wxUint8* data_ptr = start_point - slice_no*256;
      wxInt16* data_ptr_16 = start_point_16 - slice_no*256;
      for (int row_no = 0; row_no < dim[1]; row_no += factor[1]) {
        for (int col_no = 0; col_no < dim[0]; col_no += factor[0]) {
          if (write_vmr) {
            *data_ptr = std::min<wxUint8>(char_max, 
                                          static_cast<wxUint8>((*source_ptr) * scaling - data_min * scaling));
          }
          if (write_v16) { *data_ptr_16 = *source_ptr; }
          source_ptr += factor[0];
          data_ptr += 256*256;
          data_ptr_16 += 256*256;
        }
        data_ptr -= 256*256*(dim[0]/factor[0]) - 1;
        data_ptr_16 -= 256*256*(dim[0]/factor[0]) - 1;
        source_ptr += dim[0] * (factor[1] - 1);
      }
      ++slice_no;
      for (int i = 0; i < factor[2]; ++i, ++it);
    }

    wxFileName file = GetFileName(handler->GetSeriesUid());
    wxFileName::Mkdir(file.GetPath(wxPATH_GET_VOLUME), 0777, wxPATH_MKDIR_FULL);

    file.SetExt(_T(""));
    BvVmr vmr_file(static_cast<const char*>(file.GetFullPath()));  //.mb_str(wxConvLocal)));
  
    std::vector<wxUint16> dimensions;
    dimensions.push_back(256);
    dimensions.push_back(256);
    dimensions.push_back(256);

    if (write_vmr) { vmr_file.WriteVmr(dimensions, data); }
    if (write_v16) { vmr_file.WriteV16(dimensions, data_16); }
  } 


  template<class T> void
  NewBvOutputter::mConvertStc(SeriesHandler* handler)
  {
    typedef std::map <VolId, Volume<T> > vMapType;
    vMapType volumes;
    handler->GetVolumes(volumes);

    // This could cause problems, but shouldn't ever happen
    if (typeid(T) != typeid(wxUint16)) {
      wxLogError(_T("Warning: data isn't 16 bit."));
    }

    int n_volumes = handler->GetNumberOfVolumes();
    int n_slices = handler->GetNumberOfSlices();
    int n_rows = handler->GetRows();
    int n_cols = handler->GetColumns();

    // initialize time-course vectors
    std::vector< std::vector<wxUint16> > time_courses(n_slices, std::vector<wxUint16>(n_volumes*n_rows*n_cols));

    std::vector< std::vector<wxUint16> >::iterator tc_slice_it = time_courses.begin();

    for(int i = 0; i < n_slices; ++i) {
  
      typename vMapType::iterator v_it = volumes.begin();
      for (int n = 0; n < mSkip; ++n, ++v_it);
      std::vector<wxUint16>::iterator tc_it = (*tc_slice_it).begin();

      for (int j = mSkip; j < n_volumes; ++j) {
      
        std::copy((*v_it).second.GetSlice(i).begin(), 
                  (*v_it).second.GetSlice(i).end(), tc_it);

        tc_it += n_rows*n_cols;
        ++v_it;
      }

      ++tc_slice_it;

      //   wxFileName file;// = mOutputList.GetFileName(v_it->first);
      wxFileName file = GetFileName(handler->GetSeriesUid());
      wxFileName::Mkdir(file.GetPath(wxPATH_GET_VOLUME), 0777, wxPATH_MKDIR_FULL);

      file.SetExt(_T(""));
      std::string namebase(static_cast<const char*>(file.GetFullPath()));  //.mb_str(wxConvLocal)));
      // reverse slice order as requested by Yoshiko
      //    string fname = namebase + "_" + itos(i+1);
      int reversed_slice_number = n_slices - i;
      int ndigits = itos(n_slices).size();
      std::string fname = namebase + "_" + itos(reversed_slice_number, ndigits);

      //    if (Validate((dirPair.second + fname + ".stc").c_str()) == wxID_YES) {
      BvStc stc_file(fname);
      stc_file.WriteStc(n_rows, n_cols, time_courses[i]);
      //    }
    }
  }
}

#endif

