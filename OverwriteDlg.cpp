#if defined(__WXGTK__) || defined(__WXMOTIF__)
	#include <wx/wx.h>
#endif

#include <wx/wxprec.h>

#include "OverwriteDlg.h"

using namespace jcs;

BEGIN_EVENT_TABLE(OverwriteDlg, wxDialog)

	EVT_BUTTON(wxID_YES,		OverwriteDlg::OnButton)
	EVT_BUTTON(wxID_YESTOALL,	OverwriteDlg::OnButton)
	EVT_BUTTON(wxID_NO,			OverwriteDlg::OnButton)
	EVT_BUTTON(wxID_CANCEL,		OverwriteDlg::OnButton)

END_EVENT_TABLE()

OverwriteDlg::OverwriteDlg(wxWindow* parent, const char* filename)
: wxDialog(parent, -1, wxString(_T("Confirm file overwrite")))
{
	wxBoxSizer* dlgSizer = new wxBoxSizer(wxVERTICAL);

// if we add this back, might not be ok in unicode -- check
	wxString message = wxString::Format(_T("%s exists. Overwrite?"), filename);
	dlgSizer->Add(new wxStaticText(this, -1, message), 0, wxALL, 20);

	wxBoxSizer* buttonSizer = new wxBoxSizer(wxHORIZONTAL);

	buttonSizer->Add(new wxButton(this, wxID_YES, _("Yes")));
	buttonSizer->Add(0, 0, 1);
	buttonSizer->Add(new wxButton(this, wxID_YESTOALL, _("Yes to All")));
	buttonSizer->Add(0, 0, 1);
	buttonSizer->Add(new wxButton(this, wxID_NO, _("No")));
	buttonSizer->Add(0, 0, 1);
	buttonSizer->Add(new wxButton(this, wxID_CANCEL, _("Cancel")));

	dlgSizer->Add(buttonSizer, 0, wxALL|wxEXPAND, 20);

	SetSizer(dlgSizer);
	dlgSizer->Fit(this);

}

void
OverwriteDlg::OnButton(wxCommandEvent& event)
{
	EndModal(event.GetId());
}
