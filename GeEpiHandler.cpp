/// GeEpiHandler.cpp
/**
*/

#include <wx/filename.h>
#include <wx/stdpaths.h>

#include <algorithm>
#include <set>
#include <math.h>

#include "Dictionary.h"
#include "DicomFile.h"
#include "StringConvert.h"
#include "Volume.h"
#include "SeriesHandler.h"
#include "GeEpiHandler.h"

using namespace jcs;

///
/**
*/ 
GeEpiHandler::GeEpiHandler(const std::string& seriesUid)
: SeriesHandler(seriesUid)
{
}


///
/**
*/ 
SeriesHandler::VolListType
GeEpiHandler::ReadVolIds(DicomFile& file)
{ 
  VolListType v = SeriesHandler::ReadVolIds(file);

  int image_number, number_of_slices;

  if (!file.Find("InstanceNumber", image_number)) {
    wxLogError(_("Instance number not found"));
  }

  Dictionary* Excite = Excite_Dictionary::Instance();
  if (!file.Find(Excite->Lookup("Locations_in_acquisition"), number_of_slices)) {
    wxLogError(_("Locations_in_acquisition not found"));
  }

  int vol_no = static_cast<int>(floor(static_cast<double>((image_number - 1))
  /number_of_slices) + 1);

  v.front().ids.push_back(itos(vol_no, 3));

  return v;

}
