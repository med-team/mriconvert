/// NiftiOptionsDlg.h
/**
  */

#ifndef NIFTI_OPTIONS_DLG_H_
#define NIFTI_OPTIONS_DLG_H_

#include "BasicOptionsDlg.h"

namespace jcs {

  class NiftiOutputterBase;
  
  class NiftiOptionsDlg : public BasicOptionsDlg
  {
  public:
    NiftiOptionsDlg(NiftiOutputterBase* outputter);

    void OnOkay(wxCommandEvent& event);
    bool NeedsRebuild() const { return mNeedsRebuild; }
    bool SaveNii() const { return mpNiiCheck->GetValue(); }

  protected:
    DECLARE_EVENT_TABLE()

  private:
    wxCheckBox* mpNiiCheck;
	wxCheckBox* mpSaveUint16Check;
    bool mNeedsRebuild;
    bool mSaveNii;
    bool mSplit;
    int mDim;
    int mSkip;

    NiftiOutputterBase* mOutputter;


  };
}

#endif
