/// MetaHeader.cpp
/**
*/

#include "MetaHeader.h"

using namespace jcs;

bool
MetaHeader::IsMetaHeaderElement(DicomElementInstance de)
{
  bool retval = false;
  if (de.tag.group == 0x0002) {
    retval = true;
  }
  return retval;
}


///
/** Gets the transfer syntax for the main part of the file.
*/
wxUint8
MetaHeader::GetMainTransferSyntaxCode()
{
  wxUint8 retval = LEI_CODE; // Default transfer syntax for DICOM.
  std::vector<DicomElementInstance>::iterator it = mhElements.begin();
  std::vector<DicomElementInstance>::iterator it_end = mhElements.end();
  for (; it < it_end; ++it) {
    if (it->tag == DT_MFTRANSFERSYNTAX) {
      if (it->myValue->first().Cmp(wxString::FromAscii(LEE_UID))) {
        retval = LEE_CODE;
      }
      if (it->myValue->first().Cmp(wxString::FromAscii(LEI_UID))) {
        retval = LEI_CODE;
      }
      if (it->myValue->first().Cmp(wxString::FromAscii(BEE_UID))) {
        retval = BEE_CODE;
      }
      break;
    }
  }
  return retval;
}


std::istream &
jcs::operator>> (std::istream &in, MetaHeader &mh)
{
  DicomElementInstance de;
  de.transferSyntaxCode = LEE_CODE;
  std::streampos save_position;
  while (true) {
    save_position = in.tellg();
    in >> de;
    if (mh.IsMetaHeaderElement(de)) {
      mh.mhElements.push_back(de);
    }
    else {
      in.seekg(save_position);
      break;
    }
  }
  return in;
}
