/// NoOptionsDlg.h
/**
  */

#ifndef NO_OPTIONS_DLG_H_
#define NO_OPTIONS_DLG_H_

#include <wx/wx.h>
#include <wx/dialog.h>
#include <wx/panel.h>

namespace jcs {
  class NoOptionsDlg: public wxDialog
  {
  public:
    NoOptionsDlg();
    
    void OnOkay(wxCommandEvent& event);
    
  protected:
    DECLARE_EVENT_TABLE()
    
  };

}
#endif
