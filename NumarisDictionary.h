/** \file NumarisDictionary.h
    These are proprietary Numaris tags.
 */
// Identifying group    
mAddEntry ( DicomElement ( DicomTag ( 0x0009, 0x0000 ), "GROUP_LENGTH",                    "UL" ));
mAddEntry ( DicomElement ( DicomTag ( 0x0009, 0x0010 ), "PRIVATE_CREATOR",                 "LO" ));
mAddEntry ( DicomElement ( DicomTag ( 0x0009, 0x0012 ), "PRIVATE_CREATOR",                 "LO" ));
mAddEntry ( DicomElement ( DicomTag ( 0x0009, 0x0013 ), "PRIVATE_CREATOR",                 "LO" ));
mAddEntry ( DicomElement ( DicomTag ( 0x0009, 0x1010 ), "COMMENTS",                        "LO" ));
mAddEntry ( DicomElement ( DicomTag ( 0x0009, 0x1015 ), "UNIQUE_IDENTIFIER",               "LO" ));
mAddEntry ( DicomElement ( DicomTag ( 0x0009, 0x1040 ), "DATA_OBJECT_TYPE",                "US" ));
mAddEntry ( DicomElement ( DicomTag ( 0x0009, 0x1041 ), "DATA_OBJECT_SUBTYPE",             "SH" ));
mAddEntry ( DicomElement ( DicomTag ( 0x0009, 0x1210 ), "STORAGE_MODE",                    "CS" ));
mAddEntry ( DicomElement ( DicomTag ( 0x0009, 0x1212 ), "EVALUATION_MASK_IMAGE",           "UL" ));
mAddEntry ( DicomElement ( DicomTag ( 0x0009, 0x1226 ), "TABLE_ZERO_DATE",                 "DA" ));
mAddEntry ( DicomElement ( DicomTag ( 0x0009, 0x1227 ), "TABLE_ZERO_TIME",                 "TM" ));
mAddEntry ( DicomElement ( DicomTag ( 0x0009, 0x1316 ), "CPU_IDENTIFICATION_LABEL",        "LO" ));
mAddEntry ( DicomElement ( DicomTag ( 0x0009, 0x1320 ), "HEADER_VERSION",                  "SH" ));

// Patient group    
mAddEntry ( DicomElement ( DicomTag ( 0x0011, 0x0000 ), "GROUP_LENGTH",                    "UL" ));
mAddEntry ( DicomElement ( DicomTag ( 0x0011, 0x0010 ), "PRIVATE_CREATOR",                 "LO" ));
mAddEntry ( DicomElement ( DicomTag ( 0x0011, 0x0011 ), "PRIVATE_CREATOR",                 "LO" ));
mAddEntry ( DicomElement ( DicomTag ( 0x0011, 0x1010 ), "ORGAN",                           "LO" ));
mAddEntry ( DicomElement ( DicomTag ( 0x0011, 0x1110 ), "REGISTRATION_DATE",               "DA" ));
mAddEntry ( DicomElement ( DicomTag ( 0x0011, 0x1111 ), "REGISTRATION_TIME",               "TM" ));
mAddEntry ( DicomElement ( DicomTag ( 0x0011, 0x1123 ), "USED_PATIENT_WEIGHT",             "DS" ));

// Acquisition group    
mAddEntry ( DicomElement ( DicomTag ( 0x0019, 0x0000 ), "GROUP_LENGTH",                    "UL" ));
mAddEntry ( DicomElement ( DicomTag ( 0x0019, 0x0010 ), "PRIVATE_CREATOR",                 "LO" ));
mAddEntry ( DicomElement ( DicomTag ( 0x0019, 0x0012 ), "PRIVATE_CREATOR",                 "LO" ));
mAddEntry ( DicomElement ( DicomTag ( 0x0019, 0x0014 ), "PRIVATE_CREATOR",                 "LO" ));
mAddEntry ( DicomElement ( DicomTag ( 0x0019, 0x0015 ), "PRIVATE_CREATOR",                 "LO" ));
mAddEntry ( DicomElement ( DicomTag ( 0x0019, 0x1010 ), "NET_FREQUENCY",                   "IS" ));
mAddEntry ( DicomElement ( DicomTag ( 0x0019, 0x1020 ), "MEASUREMENT_MODE",                "CS" ));
mAddEntry ( DicomElement ( DicomTag ( 0x0019, 0x1030 ), "CALCULATION_MODE",                "CS" ));
mAddEntry ( DicomElement ( DicomTag ( 0x0019, 0x1050 ), "NOISE_LEVEL",                     "IS" ));
mAddEntry ( DicomElement ( DicomTag ( 0x0019, 0x1060 ), "NUMBER_OF_DATABYTES",             "IS" ));
mAddEntry ( DicomElement ( DicomTag ( 0x0019, 0x1070 ), "AC_SCALE_VECTOR",                 "DS" ));
mAddEntry ( DicomElement ( DicomTag ( 0x0019, 0x1080 ), "AC_ELEMENT_CONNECTED",            "LO" ));
mAddEntry ( DicomElement ( DicomTag ( 0x0019, 0x1210 ), "TOTAL_MEASUREMENT_TIME_NOM",      "DS" ));
mAddEntry ( DicomElement ( DicomTag ( 0x0019, 0x1211 ), "TOTAL_MEASUREMENT_TIME_CUR",      "DS" ));
mAddEntry ( DicomElement ( DicomTag ( 0x0019, 0x1212 ), "START_DELAY_TIME",                "DS" ));
mAddEntry ( DicomElement ( DicomTag ( 0x0019, 0x1213 ), "DWELL_TIME",                      "DS" ));
mAddEntry ( DicomElement ( DicomTag ( 0x0019, 0x1214 ), "NUMBER_OF_PHASES",                "IS" ));
mAddEntry ( DicomElement ( DicomTag ( 0x0019, 0x1216 ), "SEQUENCE_CONTROL_MASK",           "UL" ));
mAddEntry ( DicomElement ( DicomTag ( 0x0019, 0x1218 ), "MEASUREMENT_STATUS_MASK",         "UL" ));
mAddEntry ( DicomElement ( DicomTag ( 0x0019, 0x1220 ), "FOURIER_LINES_NOM",               "IS" ));
mAddEntry ( DicomElement ( DicomTag ( 0x0019, 0x1221 ), "FOURIER_LINES_CUR",               "IS" ));
mAddEntry ( DicomElement ( DicomTag ( 0x0019, 0x1226 ), "FOURIER_LINES_AFTER_ZERO",        "IS" ));
mAddEntry ( DicomElement ( DicomTag ( 0x0019, 0x1228 ), "FIRST_FOURIER_LINE",              "IS" ));
mAddEntry ( DicomElement ( DicomTag ( 0x0019, 0x1230 ), "ACQUISITION_COLUMNS",             "IS" ));
mAddEntry ( DicomElement ( DicomTag ( 0x0019, 0x1231 ), "RECONSTRUCTION_COLUMNS",          "IS" ));
mAddEntry ( DicomElement ( DicomTag ( 0x0019, 0x1240 ), "AC_ELEMENT_NUMBER",               "IS" ));
mAddEntry ( DicomElement ( DicomTag ( 0x0019, 0x1241 ), "AC_ELEMENT_SELECT_MASK",          "UL" ));
mAddEntry ( DicomElement ( DicomTag ( 0x0019, 0x1242 ), "AC_ELEMENT_DATA_MASK",            "UL" ));
mAddEntry ( DicomElement ( DicomTag ( 0x0019, 0x1243 ), "AC_ELEMENT_TO_ADC_CONNECT",       "IS" ));
mAddEntry ( DicomElement ( DicomTag ( 0x0019, 0x1245 ), "AC_ADC_PAIR_NUMBER",              "IS" ));
mAddEntry ( DicomElement ( DicomTag ( 0x0019, 0x1246 ), "AC_COMBINATION_MASK",             "UL" ));
mAddEntry ( DicomElement ( DicomTag ( 0x0019, 0x1250 ), "NUMBER_OF_AVERAGES",              "IS" ));
mAddEntry ( DicomElement ( DicomTag ( 0x0019, 0x1260 ), "FLIP_ANGLE",                      "DS" ));
mAddEntry ( DicomElement ( DicomTag ( 0x0019, 0x1270 ), "NUMBER_OF_PRESCANS",              "IS" ));
mAddEntry ( DicomElement ( DicomTag ( 0x0019, 0x1281 ), "RAW_DATA_FILTER_TYPE",            "CS" ));
mAddEntry ( DicomElement ( DicomTag ( 0x0019, 0x1283 ), "IMAGE_DATA_FILTER_TYPE",          "CS" ));
mAddEntry ( DicomElement ( DicomTag ( 0x0019, 0x1285 ), "PHASE_CORRECTION_FILTER_TYPE",    "CS" ));
mAddEntry ( DicomElement ( DicomTag ( 0x0019, 0x1287 ), "NORMALIZATION_FILTER_TYPE",       "CS" ));
mAddEntry ( DicomElement ( DicomTag ( 0x0019, 0x1290 ), "SATURATION_REGIONS",              "IS" ));
mAddEntry ( DicomElement ( DicomTag ( 0x0019, 0x1294 ), "IMAGE_ROTATION_ANGLE",            "DS" ));
mAddEntry ( DicomElement ( DicomTag ( 0x0019, 0x1296 ), "COIL_ID_MASK",                    "UL" ));
mAddEntry ( DicomElement ( DicomTag ( 0x0019, 0x1297 ), "COIL_CLASS_MASK",                 "UL" ));
mAddEntry ( DicomElement ( DicomTag ( 0x0019, 0x1298 ), "COIL_POSITION",                   "DS" ));
mAddEntry ( DicomElement ( DicomTag ( 0x0019, 0x1412 ), "MAGNETIC_FIELD_STRENGTH",         "DS" ));
mAddEntry ( DicomElement ( DicomTag ( 0x0019, 0x1414 ), "ADC_VOLTAGE",                     "DS" ));
mAddEntry ( DicomElement ( DicomTag ( 0x0019, 0x1416 ), "ADC_OFFSET",                      "DS" ));
mAddEntry ( DicomElement ( DicomTag ( 0x0019, 0x1420 ), "TRANSMITTER_AMPLITUDE",           "DS" ));
mAddEntry ( DicomElement ( DicomTag ( 0x0019, 0x1421 ), "NUMBER_OF_TRANS_AMPS",            "IS" ));
mAddEntry ( DicomElement ( DicomTag ( 0x0019, 0x1424 ), "TRANSMITTER_CALIBRATION",         "DS" ));
mAddEntry ( DicomElement ( DicomTag ( 0x0019, 0x1451 ), "RECEIVER_AMPLIFIER_GAIN",         "DS" ));
mAddEntry ( DicomElement ( DicomTag ( 0x0019, 0x1452 ), "RECEIVER_PREAMP_GAIN",            "DS" ));
mAddEntry ( DicomElement ( DicomTag ( 0x0019, 0x1470 ), "PHASE_GRADIENT_AMPLITUDE",        "DS" ));
mAddEntry ( DicomElement ( DicomTag ( 0x0019, 0x1471 ), "READOUT_GRADIENT_AMPLITUDE",      "DS" ));
mAddEntry ( DicomElement ( DicomTag ( 0x0019, 0x1472 ), "SELECTION_GRADIENT_AMPLITUDE",    "DS" ));
mAddEntry ( DicomElement ( DicomTag ( 0x0019, 0x1480 ), "GRADIENT_DELAY_TIME",             "DS" ));
mAddEntry ( DicomElement ( DicomTag ( 0x0019, 0x1482 ), "TOTAL_GRADIENT_DELAY_TIME",       "DS" ));
mAddEntry ( DicomElement ( DicomTag ( 0x0019, 0x1490 ), "SENSITIVITY_CORRECTION_LABEL",    "LO" ));
mAddEntry ( DicomElement ( DicomTag ( 0x0019, 0x14a0 ), "RF_WATCHDOG_MASK",                "IS" ));
mAddEntry ( DicomElement ( DicomTag ( 0x0019, 0x14a2 ), "RF_POWER_ERROR_INDICATOR",        "DS" ));
mAddEntry ( DicomElement ( DicomTag ( 0x0019, 0x14a5 ), "SPECIFIC_ABSORPTION_RATE",        "DS" ));
mAddEntry ( DicomElement ( DicomTag ( 0x0019, 0x14a6 ), "SPECIFIC_ENERGY_DOSE",            "DS" ));
mAddEntry ( DicomElement ( DicomTag ( 0x0019, 0x14b0 ), "ADJUSTMENT_STATUS_MASK",          "UL" ));
mAddEntry ( DicomElement ( DicomTag ( 0x0019, 0x14d1 ), "FLOW_SENSITIVITY",                "DS" ));
mAddEntry ( DicomElement ( DicomTag ( 0x0019, 0x14d2 ), "CALCULATION_SUBMODE",             "CS" ));
mAddEntry ( DicomElement ( DicomTag ( 0x0019, 0x14d3 ), "FIELD_OF_VIEW_RATIO",             "DS" ));
mAddEntry ( DicomElement ( DicomTag ( 0x0019, 0x14d4 ), "BASE_RAW_MATRIX_SIZE",            "IS" ));
mAddEntry ( DicomElement ( DicomTag ( 0x0019, 0x14d5 ), "2D_PHASE_OVERSAMPLING_LINES",     "IS" ));
mAddEntry ( DicomElement ( DicomTag ( 0x0019, 0x14d6 ), "2D_PHASE_OVERSAMPLING_PART",      "IS" ));
mAddEntry ( DicomElement ( DicomTag ( 0x0019, 0x14d7 ), "ECHO_LINE_POSITION",              "IS" ));
mAddEntry ( DicomElement ( DicomTag ( 0x0019, 0x14d8 ), "ECHO_COLUMN_POSITION",            "IS" ));
mAddEntry ( DicomElement ( DicomTag ( 0x0019, 0x14d9 ), "LINES_PER_SEGMENT",               "IS" ));
mAddEntry ( DicomElement ( DicomTag ( 0x0019, 0x14da ), "PHASE_CODING_DIRECTION",          "CS" ));
mAddEntry ( DicomElement ( DicomTag ( 0x0019, 0x1510 ), "PARAMETER_FILE_NAME",             "LO" ));
mAddEntry ( DicomElement ( DicomTag ( 0x0019, 0x1511 ), "SEQUENCE_FILE_NAME",              "LO" ));
mAddEntry ( DicomElement ( DicomTag ( 0x0019, 0x1512 ), "SEQUENCE_FILE_OWNER",             "LO" ));
mAddEntry ( DicomElement ( DicomTag ( 0x0019, 0x1513 ), "SEQUENCE_DESCRIPTION",            "LO" ));

// Relationship group
mAddEntry ( DicomElement ( DicomTag ( 0x0021, 0x0000 ), "GROUP_LENGTH",                    "UL" ));
mAddEntry ( DicomElement ( DicomTag ( 0x0021, 0x0010 ), "PRIVATE_CREATOR",                 "LO" ));
mAddEntry ( DicomElement ( DicomTag ( 0x0021, 0x0011 ), "PRIVATE_CREATOR",                 "LO" ));
mAddEntry ( DicomElement ( DicomTag ( 0x0021, 0x0013 ), "PRIVATE_CREATOR",                 "LO" ));
mAddEntry ( DicomElement ( DicomTag ( 0x0021, 0x0023 ), "PRIVATE_CREATOR",                 "LO" ));
mAddEntry ( DicomElement ( DicomTag ( 0x0021, 0x1010 ), "ZOOM",                            "DS" ));
mAddEntry ( DicomElement ( DicomTag ( 0x0021, 0x1011 ), "TARGET",                          "DS" ));
mAddEntry ( DicomElement ( DicomTag ( 0x0021, 0x1020 ), "ROI_MASK",                        "US" ));
mAddEntry ( DicomElement ( DicomTag ( 0x0021, 0x1120 ), "FIELD_OF_VIEW",                   "DS" ));
mAddEntry ( DicomElement ( DicomTag ( 0x0021, 0x1122 ), "IMAGE_MAGNIFICATION_FACTOR",      "DS" ));
mAddEntry ( DicomElement ( DicomTag ( 0x0021, 0x1124 ), "IMAGE_SCROLL_OFFSET",             "DS" ));
mAddEntry ( DicomElement ( DicomTag ( 0x0021, 0x1126 ), "IMAGE_PIXEL_OFFSET",              "IS" ));
mAddEntry ( DicomElement ( DicomTag ( 0x0021, 0x1130 ), "VIEW_DIRECTION",                  "CS" ));
mAddEntry ( DicomElement ( DicomTag ( 0x0021, 0x1132 ), "REST_DIRECTION",                  "CS" ));
mAddEntry ( DicomElement ( DicomTag ( 0x0021, 0x1160 ), "IMAGE_POSITION",                  "DS" ));
mAddEntry ( DicomElement ( DicomTag ( 0x0021, 0x1161 ), "IMAGE_NORMAL",                    "DS" ));
mAddEntry ( DicomElement ( DicomTag ( 0x0021, 0x1163 ), "IMAGE_DISTANCE",                  "DS" ));
mAddEntry ( DicomElement ( DicomTag ( 0x0021, 0x1165 ), "IMAGE_POSITIONING_HISTORY_MASK",  "US" ));
mAddEntry ( DicomElement ( DicomTag ( 0x0021, 0x116a ), "IMAGE_ROW",                       "DS" ));
mAddEntry ( DicomElement ( DicomTag ( 0x0021, 0x116b ), "IMAGE_COLUMN",                    "DS" ));
mAddEntry ( DicomElement ( DicomTag ( 0x0021, 0x1170 ), "PATIENT_ORIENTATION_SET_1",       "CS" ));
mAddEntry ( DicomElement ( DicomTag ( 0x0021, 0x1171 ), "PATIENT_ORIENTATION_SET_2",       "CS" ));
mAddEntry ( DicomElement ( DicomTag ( 0x0021, 0x1182 ), "STUDY_TYPE",                      "SH" ));
mAddEntry ( DicomElement ( DicomTag ( 0x0021, 0x1320 ), "PHASE_COR_ROW_SEQ",               "IS" ));
mAddEntry ( DicomElement ( DicomTag ( 0x0021, 0x1321 ), "PHASE_COR_COL_SEQ",               "IS" ));
mAddEntry ( DicomElement ( DicomTag ( 0x0021, 0x1322 ), "PHASE_CORRECTION_ROWS",           "IS" ));
mAddEntry ( DicomElement ( DicomTag ( 0x0021, 0x1324 ), "PHASE_CORRECTION_COLUMNS",        "IS" ));
mAddEntry ( DicomElement ( DicomTag ( 0x0021, 0x1330 ), "3D_RAW_PARTITIONS_NOMINAL",       "IS" ));
mAddEntry ( DicomElement ( DicomTag ( 0x0021, 0x1331 ), "3D_RAW_PARTITIONS_CURRENT",       "IS" ));
mAddEntry ( DicomElement ( DicomTag ( 0x0021, 0x1334 ), "IMAGE_PARTITIONS",                "IS" ));
mAddEntry ( DicomElement ( DicomTag ( 0x0021, 0x1336 ), "PARTITION_NUMBER",                "IS" ));
mAddEntry ( DicomElement ( DicomTag ( 0x0021, 0x1339 ), "SLAB_THICKNESS",                  "DS" ));
mAddEntry ( DicomElement ( DicomTag ( 0x0021, 0x1340 ), "NUMBER_OF_SLICES_NOMINAL",        "IS" ));
mAddEntry ( DicomElement ( DicomTag ( 0x0021, 0x1341 ), "NUMBER_OF_SLICES_CURRENT",        "IS" ));
mAddEntry ( DicomElement ( DicomTag ( 0x0021, 0x1342 ), "CURRENT_SLICE_NUMBER",            "IS" ));
mAddEntry ( DicomElement ( DicomTag ( 0x0021, 0x1343 ), "CURRENT_GROUP_NUMBER",            "IS" ));
mAddEntry ( DicomElement ( DicomTag ( 0x0021, 0x1344 ), "CURRENT_SLICE_DISTANCE_FACTOR",   "DS" ));
mAddEntry ( DicomElement ( DicomTag ( 0x0021, 0x134f ), "ORDER_OF_SLICES",                 "ST" ));
mAddEntry ( DicomElement ( DicomTag ( 0x0021, 0x1350 ), "SIGNAL_MASK",                     "IS" ));
mAddEntry ( DicomElement ( DicomTag ( 0x0021, 0x1356 ), "EFFECTIVE_REPETITION_TIME",       "DS" ));
mAddEntry ( DicomElement ( DicomTag ( 0x0021, 0x1370 ), "NUMBER_OF_ECHOES",                "IS" ));
mAddEntry ( DicomElement ( DicomTag ( 0x0021, 0x2300 ), "SEQUENCE_TYPE",                   "CS" ));
mAddEntry ( DicomElement ( DicomTag ( 0x0021, 0x2301 ), "VECTOR_SIZE_ORIGINAL",            "IS" ));
mAddEntry ( DicomElement ( DicomTag ( 0x0021, 0x2302 ), "VECTOR_SIZE_EXTENDED",            "IS" ));
mAddEntry ( DicomElement ( DicomTag ( 0x0021, 0x2303 ), "ACQUIRED_SPECTRAL_RANGE",         "DS" ));
mAddEntry ( DicomElement ( DicomTag ( 0x0021, 0x2304 ), "VOI_POSITION",                    "DS" ));
mAddEntry ( DicomElement ( DicomTag ( 0x0021, 0x2305 ), "VOI_SIZE",                        "DS" ));
mAddEntry ( DicomElement ( DicomTag ( 0x0021, 0x2306 ), "CSI_MATRIX_SIZE_ORIGINAL",        "IS" ));
mAddEntry ( DicomElement ( DicomTag ( 0x0021, 0x2307 ), "CSI_MATRIX_SIZE_EXTENDED",        "IS" ));
mAddEntry ( DicomElement ( DicomTag ( 0x0021, 0x2308 ), "SPATIAL_GRID_SHIFT",              "DS" ));
mAddEntry ( DicomElement ( DicomTag ( 0x0021, 0x2309 ), "SIGNAL_LIMITS_MINIMUM",           "DS" ));
mAddEntry ( DicomElement ( DicomTag ( 0x0021, 0x2310 ), "SIGNAL_LIMITS_MAXIMUM",           "DS" ));
mAddEntry ( DicomElement ( DicomTag ( 0x0021, 0x2311 ), "SPECTROSCOPY_INFO_MASK",          "IS" ));
mAddEntry ( DicomElement ( DicomTag ( 0x0021, 0x2330 ), "AC_ADC_OFFSET",                   "DS" ));
mAddEntry ( DicomElement ( DicomTag ( 0x0021, 0x2331 ), "AC_PREAMPLIFIER_GAIN",            "DS" ));

//  Image presentation group
mAddEntry ( DicomElement ( DicomTag ( 0x0029, 0x0000 ), "GROUP_LENGTH",                    "UL" ));
mAddEntry ( DicomElement ( DicomTag ( 0x0029, 0x0010 ), "PRIVATE_CREATOR",                 "LO" ));
mAddEntry ( DicomElement ( DicomTag ( 0x0029, 0x0011 ), "PRIVATE_CREATOR",                 "LO" ));
mAddEntry ( DicomElement ( DicomTag ( 0x0029, 0x0012 ), "PRIVATE_CREATOR",                 "LO" ));
mAddEntry ( DicomElement ( DicomTag ( 0x0029, 0x1010 ), "CSAImageHeader",                  "OB" ));
mAddEntry ( DicomElement ( DicomTag ( 0x0029, 0x1110 ), "WINDOW_STYLE",                    "CS" ));
mAddEntry ( DicomElement ( DicomTag ( 0x0029, 0x1010 ), "CSASeriesHeader",                 "OB" ));
mAddEntry ( DicomElement ( DicomTag ( 0x0029, 0x1120 ), "PIXEL_QUALITY_CODE",              "CS" ));
mAddEntry ( DicomElement ( DicomTag ( 0x0029, 0x1152 ), "SORT_CODE",                       "IS" ));
mAddEntry ( DicomElement ( DicomTag ( 0x0029, 0x1131 ), "PMTF_INFORMATION_1",              "LO" ));
mAddEntry ( DicomElement ( DicomTag ( 0x0029, 0x1132 ), "PMTF_INFORMATION_2",              "UL" ));
mAddEntry ( DicomElement ( DicomTag ( 0x0029, 0x1133 ), "PMTF_INFORMATION_3",              "UL" ));
mAddEntry ( DicomElement ( DicomTag ( 0x0029, 0x1134 ), "PMTF_INFORMATION_4",              "CS" ));

// Device group
mAddEntry ( DicomElement ( DicomTag ( 0x0051, 0x0000 ), "GROUP_LENGTH",                    "UL" ));
mAddEntry ( DicomElement ( DicomTag ( 0x0051, 0x0010 ), "PRIVATE_CREATOR",                 "LO" ));
mAddEntry ( DicomElement ( DicomTag ( 0x0051, 0x1010 ), "IMAGE_TEXT",                      "LO" ));
/**
https://groups.google.com/forum/?hl=en&fromgroups=#!searchin/comp.protocols.dicom/siemens$20csa$20header/comp.protocols.dicom/c7OC15FXKGA/gwjt_nJbmncJ
         "AbsTablePosition",
                "AcquisitionMatrixText",
                "AcqusitionMatrix",
                "Actual3DImaPartNumber",
                "AutoAlignMatrix",
                "B_value",
                "CoilForGradient",
                "CoilId",
                "CoilString",
                "CoilTuningReflection",
                "Columns",
                "Comp_AdjustedParam",
                "Comp_Algorithm",
                "Comp_AutoParam",
                "Comp_Blended",
                "Comp_JobID",
                "Comp_ManualAdjusted",
                "CsiGridshiftVector",
                "DataFile",
                "DataFileName",
                "DiffusionDirectionality",
                "DiffusionGradientDirection",
                "EchoColumnPosition",
                "EchoLinePosition",
                "EchoNumbers",
                "EchoPartitionPosition",
                "EchoTime",
                "EchoTrainLength",
                "FMRIStimulInfo",
                "Filter1",
                "Filter2",
                "FlipAngle",
                "FlowEncodingDirection",
                "FlowEncodingDirectionString",
                "FlowVenc",
                "GradientDelayTime",
                "ICE_Dims",
                "ImageComments",
                "ImageGroup",
                "ImageNumber",
                "ImageOrientationPatient",
                "ImagePositionPatient",
                "ImagedNucleus",
                "ImagingFrequency",
                "InversionTime",
                "LongModelName",
                "MagneticFieldStrength",
                "MeasuredFourierLines",
                "MeasurementIndex",
                "MiscSequenceParam",
                "MrEvaProtocol",
                "MrProtocol",
                "MrProtocolVersion",
                "NumberOfAverages",
                "NumberOfImagesInMosaic",
                "NumberOfPhaseEncodingSteps",
                "NumberOfPrescans",
                "Operation_mode_flag",
                "PATModeText",
                "PatReinPattern",
                "PatientOrientation",
                "PercentPhaseFieldOfView",
                "PercentSampling",
                "PhaseEncodingDirection",
                "PhaseEncodingDirectionPositive",
                "PhaseGradientAmplitude",
                "PixelBandwidth",
                "PixelFile",
                "PixelFileName",
                "PixelSpacing",
                "PositivePCSDirections",
                "ProtocolSliceNumber",
                "ReadoutGradientAmplitude",
                "ReadoutOS",
                "RealDwellTime",
                "ReceivingCoil",
                "ReferencedImageSequence",
                "RelTablePosition",
                "RepetitionTime",
                "RepetitionTimeEffective",
                "RepresentativeImage",
                "RfPowerErrorIndicator",
                "RfWatchdogMask",
                "Rows",
                "SAR",
                "SW_korr_faktor",
                "SarWholeBody",
                "ScanningSequence",
                "Sed",
                "SelectionGradientAmplitude",
                "SequenceFileOwner",
                "SequenceMask",
                "SequenceName",
                "SliceArrayConcatenations",
                "SliceLocation",
                "SliceMeasurementDuration",
                "SliceNormalVector",
                "SliceResolution",
                "SliceThickness",
                "SourceImageSequence",
                "SpectrumTextRegionLabel",
                "Stim_faktor",
                "Stim_lim",
                "Stim_max_ges_norm_online",
                "Stim_max_online",
                "Stim_mon_mode",
                "TimeAfterStart",
                "TransmitterCalibration",
                "TransmittingCoil",
                "TriggerTime",
                "UsedChannelMask",
                "UsedPatientWeight",
                "VariableFlipAngleFlag",
                "VoxelInPlaneRot",
                "VoxelNormalCor",
                "VoxelNormalSag",
                "VoxelNormalTra",
                "VoxelPhaseFOV",
                "VoxelPositionCor",
                "VoxelPositionSag",
                "VoxelPositionTra",
                "VoxelReadoutFOV",
                "VoxelThickness",
                "dBdt",
                "dBdt_limit",
                "dBdt_max",
                "dBdt_thresh",
                "t_puls_max",

**/
