/// BvOptionsDlg.h
/**
  */

#ifndef BV_OPTIONS_DLG_H_
#define BV_OPTIONS_DLG_H_

#include <wx/wx.h>
#include <wx/dialog.h>
#include <wx/panel.h>

#include "OutputterBase.h"
#include "NewBvOutputter.h"

namespace jcs {

  class BvOptionsDlg : public wxDialog
  {
  public:
    BvOptionsDlg(NewBvOutputter* outputter);

    void OnOkay(wxCommandEvent& event);
    bool NeedsRebuild() const { return mNeedsRebuild; }
    bool SplitDirs() const;
    bool SplitSubj() const;
    bool SaveV16() const;
    int Skip() const;

  protected:
    DECLARE_EVENT_TABLE()

      private:
    wxSpinCtrl* mpBvSpin;
    wxCheckBox* mpBvCheck;
    wxCheckBox* mpSplitDirsCheck;
    wxCheckBox* mpSplitSubjCheck;
    wxCheckListBox* mpNameFields;
    bool mNeedsRebuild;
    bool mSplit;
    bool mSaveV16;
    int mSkip;
    NewBvOutputter* mOutputter;

    bool SaveNameFields();
  };

}
#endif
