/// OverrideDlg.cpp
/**
*/

#include <wx/wxprec.h>
#include <wx/spinctrl.h>
#include <wx/statline.h>

#include "OverrideDlg.h"

using namespace jcs;

OverrideDlg::OverrideDlg()
: wxDialog(NULL, -1, wxString(_("Override defaults")))
{
  wxBoxSizer* dlgSizer = new wxBoxSizer(wxVERTICAL);

  dimCheck = new wxCheckBox(this, -1, _("Save this series as 4d"));
  dlgSizer->Add(dimCheck, 0, wxALL, 10);

  dlgSizer->Add(new wxStaticText(this, -1, _("Skip volumes for multivolume series:")),
  0, wxLEFT|wxRIGHT|wxBOTTOM, 10);

  skipSpin = new wxSpinCtrl(this);
  dlgSizer->Add(skipSpin, 0, wxLEFT|wxRIGHT|wxBOTTOM, 10);

  wxBoxSizer* buttonSizer = new wxBoxSizer(wxHORIZONTAL);

  wxButton* okayButton = new wxButton(this, wxID_OK, _("Okay"));
  buttonSizer->Add(okayButton, 0, wxRIGHT, 10);
  buttonSizer->Add(new wxButton(this, wxID_CANCEL, _("Cancel")));
  okayButton->SetDefault();

  dlgSizer->Add(buttonSizer, 0, wxALIGN_RIGHT|wxALL, 10);

  SetSizer(dlgSizer);
  dlgSizer->Fit(this);
}
