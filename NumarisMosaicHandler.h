/// NumarisMosaicHandler.h
/**
  */

#ifndef NUMARIS_MOSAIC_HANDLER_H
#define NUMARIS_MOSAIC_HANDLER_H

#include "SeriesHandler.h"

namespace jcs {
  ///
  /**
    */
  class NumarisMosaicHandler : public SeriesHandler
  {
    public :
    NumarisMosaicHandler(const std::string& seriesUid);
    

    virtual int GetNumberOfSlices() const;
    virtual std::vector<double> GetVoxelSize();
    virtual int GetRows();
    virtual int GetColumns();
    virtual AoCode GetAcquisitionOrder() const;
    virtual double GetVolumeInterval() const;
    virtual std::vector<std::string> GetStringInfo();
    virtual bool IsMosaic() const { return true; }

    virtual void GetVolumes (std::map <VolId, Volume <wxInt64> >& v) { mGetVolumes <wxInt64> (v); } 
    virtual void GetVolumes (std::map <VolId, Volume <wxUint64> >& v) { mGetVolumes <wxUint64> (v); } 
    virtual void GetVolumes (std::map <VolId, Volume <wxInt32> >& v) { mGetVolumes <wxInt32> (v); } 
    virtual void GetVolumes (std::map <VolId, Volume <wxUint32> >& v) { mGetVolumes <wxUint32> (v); } 
    virtual void GetVolumes (std::map <VolId, Volume <wxInt16> >& v) { mGetVolumes <wxInt16> (v); } 
    virtual void GetVolumes (std::map <VolId, Volume <wxUint16> >& v) { mGetVolumes <wxUint16> (v); } 
    virtual void GetVolumes (std::map <VolId, Volume <wxInt8> >& v) { mGetVolumes <wxInt8> (v); }
    virtual void GetVolumes (std::map <VolId, Volume <wxUint8> >& v) { mGetVolumes <wxUint8> (v); } 

    protected :
    virtual std::vector<double> GetSliceOrder();
    virtual VolListType ReadVolIds(DicomFile& file);

  private:
    template <class T> void
    mGetVolumes(std::map<VolId, Volume<T> >& v);

  };
};

#endif
