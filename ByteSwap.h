#ifndef BYTESWAP_H_
#define BYTESWAP_H_


/* 

This function template byte swap numbers or elements in 
an array. The first argument is a reference to the number or array; 
the second argument is the size of the array elements that need 
to be rearranged.

*/

namespace jcs {
template <class T>
void ByteSwap(T& rBuffer, int n) { 

	char* bp = reinterpret_cast<char*>(&rBuffer);
	char* sp = new char[n];

	for (int j = 0; j < sizeof(rBuffer); j += n) {
		for (int i = 0; i < n; ++i)
			*sp++ = *bp++;

		bp -= n;
		for (int j = 0; j < n; ++j)
			*bp++ = *--sp;

	}
		
	delete sp;
  
}

}

#endif
